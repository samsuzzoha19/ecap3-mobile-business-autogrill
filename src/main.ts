// this import should be first in order to load some required settings (like globals and reflect-metadata)
import { platformNativeScriptDynamic } from "nativescript-angular/platform";
// import { setCssFileName, start as applicationStart } from "tns-core-modules/application";
// setCssFileName("theme-style/styles.sass");

import { RootModule } from "./root/root.module";
platformNativeScriptDynamic({createFrameOnBootstrap: false}).bootstrapModule(RootModule);