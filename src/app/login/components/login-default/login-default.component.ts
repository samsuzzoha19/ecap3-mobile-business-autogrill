import {Component, OnInit, OnDestroy} from "@angular/core";
import { Subscription } from "rxjs";
import {LoginService} from '@ecap3/tns-platform-login';
import { PlatformGdprService } from "@ecap3/tns-platform-gdpr";
import { NavigationProvider, UserInfoService } from "@ecap3/tns-core";
import { LoginConfiguration } from "@ecap3/tns-platform-login/components/login-default/login-default.component";

@Component({
    selector: "app-login",
    moduleId: module.id,
    templateUrl: "./login-default.component.html",
    styleUrls: [
        "./login-default-common.scss",
        "./login-default.component.scss"
    ]
})
export class LoginDefaultComponent implements OnInit, OnDestroy {
    showGDPR = false;
    requiredConditionsDetails: any;
    getGdprContentUsingInterface: boolean = false;

    subscription: Subscription[] = [];

    loginDefaultRoutes: any[] = [
        {
            role: 'client_specific',
            route: '/dashboard'
        },
        {
            role: 'admin',
            route: '/dashboard'
        },
        {
            role: 'general-user',
            route: '/dashboard'
        },
        {
            role: 'project-manager',
            route: '/dashboard'
        }
    ];
    loginPageData: LoginConfiguration =
        {
            loginHeaderText: null,
            socialLogin: false,
            createAccount: false,
            rememberMe: false,
            forgotPassword: true,
            languageOption: true,
            showBackgroundAnimation: true,
            backgroundImageSrc: "res://login_bg",
            topLogo: {
                src: "res://selise_logo"
            },
            bottomlogo: {
                src: "",
                url: "",
            },
            centerText: false,
            topText: {
                welcomeText: '',
                welcomeDescription: "",
            },
            leftSideLogo: null,
            termsConditons: [
                // { url1: ""},
                // { url2: ""},
                // {url3: ""}

            ],
            action: "",
            landingUri: "",
            defaultCredential: {
                email: 'wk@selise.ch',
                password: '1qazZAQ!',
            },
            loginScreenSize: {
                height: '100%'
            }
        };

    gdprConfiguration : any = {
        logo: {
            src: "res://clm_logo",
            test: ':'
        }
    };
    constructor(private loginService: LoginService, private gdprService: PlatformGdprService,
        private navigationProvider: NavigationProvider,
        private userInfoService: UserInfoService) {
        console.log('LoginComponent Biz App!!');

    }

    ngOnInit() {
        console.log('Login Page Loaded');

        this.subscription.push(
            this.loginService.gdprControl.subscribe((errorData) => {
                if(this.showGDPR) {
                    this.showGDPR = false;
                }

                setTimeout(()=>{
                    this.showGDPR = true;
                }, 1);

                this.requiredConditionsDetails = errorData;
            })
        );

        this.subscription.push(
            this.gdprService.gdprEvent.subscribe((response) => {
                console.log('data response', response);
                if (response && response.access_token){
                    this.setRouteSessionAfterLogin(response);
                }
                this.showGDPR = false;
            })
        );
    }

    public setRouteSessionAfterLogin(response: any): void {
        const userDetails = this.userInfoService.getUserDataByAccessToken(response.access_token);
        this.navigationProvider.assignNavigationsVisibility();
        this.navigationProvider.getCurrentNavigationObservable().subscribe((data) => {
            if (data && data.length > 0) {
                this.userInfoService.setUserInfo(userDetails);
                this.userInfoService.sendUserInfoEvent(userDetails);
            }
        });
    }

    ngOnDestroy() {
        if (this.subscription.length > 0) {
            this.subscription.forEach(item => item.unsubscribe());
        }
    }
}
