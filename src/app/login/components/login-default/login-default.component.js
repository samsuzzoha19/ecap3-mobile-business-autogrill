"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var tns_platform_login_1 = require("@ecap3/tns-platform-login");
var tns_platform_gdpr_1 = require("@ecap3/tns-platform-gdpr");
var tns_core_1 = require("@ecap3/tns-core");
var LoginDefaultComponent = /** @class */ (function () {
    function LoginDefaultComponent(loginService, gdprService, navigationProvider, userInfoService) {
        this.loginService = loginService;
        this.gdprService = gdprService;
        this.navigationProvider = navigationProvider;
        this.userInfoService = userInfoService;
        this.showGDPR = false;
        this.getGdprContentUsingInterface = false;
        this.subscription = [];
        this.loginDefaultRoutes = [
            {
                role: 'client_specific',
                route: '/dashboard'
            },
            {
                role: 'admin',
                route: '/dashboard'
            },
            {
                role: 'general-user',
                route: '/dashboard'
            },
            {
                role: 'project-manager',
                route: '/dashboard'
            }
        ];
        this.loginPageData = {
            loginHeaderText: null,
            socialLogin: false,
            createAccount: false,
            rememberMe: false,
            forgotPassword: true,
            languageOption: true,
            showBackgroundAnimation: true,
            topLogo: {
                src: "res://clm_logo"
            },
            bottomlogo: {
                src: "",
                url: "",
            },
            centerText: false,
            topText: {
                welcomeText: '',
                welcomeDescription: "",
            },
            leftSideLogo: null,
            termsConditon: {
                url1: "",
                url2: "",
                url3: ""
            },
            action: "",
            landingUri: ""
        };
        this.gdprConfiguration = {
            logo: {
                src: "res://clm_logo"
            }
        };
        console.log('LoginComponent Biz App!!');
    }
    LoginDefaultComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('Login Page Loaded');
        this.subscription.push(this.loginService.gdprControl.subscribe(function (errorData) {
            if (_this.showGDPR) {
                _this.showGDPR = false;
            }
            setTimeout(function () {
                _this.showGDPR = true;
            }, 1);
            _this.requiredConditionsDetails = errorData;
        }));
        this.subscription.push(this.gdprService.gdprEvent.subscribe(function (response) {
            console.log('data response', response);
            if (response && response.access_token) {
                _this.setRouteSessionAfterLogin(response);
            }
            _this.showGDPR = false;
        }));
    };
    LoginDefaultComponent.prototype.setRouteSessionAfterLogin = function (response) {
        var _this = this;
        var userDetails = this.userInfoService.getUserDataByAccessToken(response.access_token);
        this.navigationProvider.assignNavigationsVisibility();
        this.navigationProvider.getCurrentNavigationObserable().subscribe(function (data) {
            if (data && data.length > 0) {
                _this.userInfoService.setUserInfo(userDetails);
                _this.userInfoService.sendUserInfoEvent(userDetails);
            }
        });
    };
    LoginDefaultComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.forEach(function (item) { return item.unsubscribe(); });
        }
    };
    LoginDefaultComponent = __decorate([
        core_1.Component({
            selector: "app-login",
            moduleId: module.id,
            templateUrl: "./login-default.component.html",
            styleUrls: [
                "./login-default-common.scss",
                "./login-default.component.scss"
            ]
        }),
        __metadata("design:paramtypes", [tns_platform_login_1.LoginService, tns_platform_gdpr_1.PlatformGdprService,
            tns_core_1.NavigationProvider,
            tns_core_1.UserInfoService])
    ], LoginDefaultComponent);
    return LoginDefaultComponent;
}());
exports.LoginDefaultComponent = LoginDefaultComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tZGVmYXVsdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJsb2dpbi1kZWZhdWx0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyRDtBQUUzRCxnRUFBdUQ7QUFDdkQsOERBQStEO0FBQy9ELDRDQUFzRTtBQVl0RTtJQThESSwrQkFBb0IsWUFBMEIsRUFBVSxXQUFnQyxFQUM1RSxrQkFBc0MsRUFDdEMsZUFBZ0M7UUFGeEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBcUI7UUFDNUUsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUEvRDVDLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFFakIsaUNBQTRCLEdBQVksS0FBSyxDQUFDO1FBRTlDLGlCQUFZLEdBQW1CLEVBQUUsQ0FBQztRQUVsQyx1QkFBa0IsR0FBVTtZQUN4QjtnQkFDSSxJQUFJLEVBQUUsaUJBQWlCO2dCQUN2QixLQUFLLEVBQUUsWUFBWTthQUN0QjtZQUNEO2dCQUNJLElBQUksRUFBRSxPQUFPO2dCQUNiLEtBQUssRUFBRSxZQUFZO2FBQ3RCO1lBQ0Q7Z0JBQ0ksSUFBSSxFQUFFLGNBQWM7Z0JBQ3BCLEtBQUssRUFBRSxZQUFZO2FBQ3RCO1lBQ0Q7Z0JBQ0ksSUFBSSxFQUFFLGlCQUFpQjtnQkFDdkIsS0FBSyxFQUFFLFlBQVk7YUFDdEI7U0FDSixDQUFDO1FBQ0Ysa0JBQWEsR0FDVDtZQUNJLGVBQWUsRUFBRSxJQUFJO1lBQ3JCLFdBQVcsRUFBRSxLQUFLO1lBQ2xCLGFBQWEsRUFBRSxLQUFLO1lBQ3BCLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLGNBQWMsRUFBRSxJQUFJO1lBQ3BCLGNBQWMsRUFBRSxJQUFJO1lBQ3BCLHVCQUF1QixFQUFFLElBQUk7WUFDN0IsT0FBTyxFQUFFO2dCQUNMLEdBQUcsRUFBRSxnQkFBZ0I7YUFDeEI7WUFDRCxVQUFVLEVBQUU7Z0JBQ1IsR0FBRyxFQUFFLEVBQUU7Z0JBQ1AsR0FBRyxFQUFFLEVBQUU7YUFDVjtZQUNELFVBQVUsRUFBRSxLQUFLO1lBQ2pCLE9BQU8sRUFBRTtnQkFDTCxXQUFXLEVBQUUsRUFBRTtnQkFDZixrQkFBa0IsRUFBRSxFQUFFO2FBQ3pCO1lBQ0QsWUFBWSxFQUFFLElBQUk7WUFDbEIsYUFBYSxFQUFFO2dCQUNYLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2dCQUNSLElBQUksRUFBRSxFQUFFO2FBRVg7WUFDRCxNQUFNLEVBQUUsRUFBRTtZQUNWLFVBQVUsRUFBRSxFQUFFO1NBQ2pCLENBQUE7UUFFTCxzQkFBaUIsR0FBUztZQUN0QixJQUFJLEVBQUU7Z0JBQ0YsR0FBRyxFQUFFLGdCQUFnQjthQUN4QjtTQUNKLENBQUE7UUFJRyxPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixDQUFDLENBQUM7SUFFNUMsQ0FBQztJQUVELHdDQUFRLEdBQVI7UUFBQSxpQkEwQkM7UUF6QkcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBRWpDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUNsQixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsVUFBQyxTQUFTO1lBQzlDLElBQUcsS0FBSSxDQUFDLFFBQVEsRUFBRTtnQkFDZCxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUN6QjtZQUVELFVBQVUsQ0FBQztnQkFDUCxLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztZQUN6QixDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFFTixLQUFJLENBQUMseUJBQXlCLEdBQUcsU0FBUyxDQUFDO1FBQy9DLENBQUMsQ0FBQyxDQUNMLENBQUM7UUFFRixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDbEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFVBQUMsUUFBUTtZQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUN2QyxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsWUFBWSxFQUFDO2dCQUNsQyxLQUFJLENBQUMseUJBQXlCLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDNUM7WUFDRCxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUMxQixDQUFDLENBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQztJQUVNLHlEQUF5QixHQUFoQyxVQUFpQyxRQUFhO1FBQTlDLGlCQVNDO1FBUkcsSUFBTSxXQUFXLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyx3QkFBd0IsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDekYsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDJCQUEyQixFQUFFLENBQUM7UUFDdEQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLDZCQUE2QixFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUMsSUFBSTtZQUNuRSxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDekIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzlDLEtBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDdkQ7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwyQ0FBVyxHQUFYO1FBQ0ksSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFsQixDQUFrQixDQUFDLENBQUM7U0FDekQ7SUFDTCxDQUFDO0lBaEhRLHFCQUFxQjtRQVRqQyxnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLFdBQVc7WUFDckIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSxnQ0FBZ0M7WUFDN0MsU0FBUyxFQUFFO2dCQUNQLDZCQUE2QjtnQkFDN0IsZ0NBQWdDO2FBQ25DO1NBQ0osQ0FBQzt5Q0ErRG9DLGlDQUFZLEVBQXVCLHVDQUFtQjtZQUN4RCw2QkFBa0I7WUFDckIsMEJBQWU7T0FoRW5DLHFCQUFxQixDQWlIakM7SUFBRCw0QkFBQztDQUFBLEFBakhELElBaUhDO0FBakhZLHNEQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBPbkluaXQsIE9uRGVzdHJveX0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSBcInJ4anNcIjtcclxuaW1wb3J0IHtMb2dpblNlcnZpY2V9IGZyb20gJ0BlY2FwMy90bnMtcGxhdGZvcm0tbG9naW4nO1xyXG5pbXBvcnQgeyBQbGF0Zm9ybUdkcHJTZXJ2aWNlIH0gZnJvbSBcIkBlY2FwMy90bnMtcGxhdGZvcm0tZ2RwclwiO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uUHJvdmlkZXIsIFVzZXJJbmZvU2VydmljZSB9IGZyb20gXCJAZWNhcDMvdG5zLWNvcmVcIjtcclxuaW1wb3J0IHsgTG9naW5Db25maWd1cmF0aW9uIH0gZnJvbSBcIkBlY2FwMy90bnMtcGxhdGZvcm0tbG9naW4vY29tcG9uZW50cy9sb2dpbi1kZWZhdWx0L2xvZ2luLWRlZmF1bHQuY29tcG9uZW50XCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiBcImFwcC1sb2dpblwiLFxyXG4gICAgbW9kdWxlSWQ6IG1vZHVsZS5pZCxcclxuICAgIHRlbXBsYXRlVXJsOiBcIi4vbG9naW4tZGVmYXVsdC5jb21wb25lbnQuaHRtbFwiLFxyXG4gICAgc3R5bGVVcmxzOiBbXHJcbiAgICAgICAgXCIuL2xvZ2luLWRlZmF1bHQtY29tbW9uLnNjc3NcIixcclxuICAgICAgICBcIi4vbG9naW4tZGVmYXVsdC5jb21wb25lbnQuc2Nzc1wiXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMb2dpbkRlZmF1bHRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgICBzaG93R0RQUiA9IGZhbHNlO1xyXG4gICAgcmVxdWlyZWRDb25kaXRpb25zRGV0YWlsczogYW55O1xyXG4gICAgZ2V0R2RwckNvbnRlbnRVc2luZ0ludGVyZmFjZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIHN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uW10gPSBbXTtcclxuXHJcbiAgICBsb2dpbkRlZmF1bHRSb3V0ZXM6IGFueVtdID0gW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgcm9sZTogJ2NsaWVudF9zcGVjaWZpYycsXHJcbiAgICAgICAgICAgIHJvdXRlOiAnL2Rhc2hib2FyZCdcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgcm9sZTogJ2FkbWluJyxcclxuICAgICAgICAgICAgcm91dGU6ICcvZGFzaGJvYXJkJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICByb2xlOiAnZ2VuZXJhbC11c2VyJyxcclxuICAgICAgICAgICAgcm91dGU6ICcvZGFzaGJvYXJkJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICByb2xlOiAncHJvamVjdC1tYW5hZ2VyJyxcclxuICAgICAgICAgICAgcm91dGU6ICcvZGFzaGJvYXJkJ1xyXG4gICAgICAgIH1cclxuICAgIF07XHJcbiAgICBsb2dpblBhZ2VEYXRhOiBMb2dpbkNvbmZpZ3VyYXRpb24gPVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgbG9naW5IZWFkZXJUZXh0OiBudWxsLFxyXG4gICAgICAgICAgICBzb2NpYWxMb2dpbjogZmFsc2UsXHJcbiAgICAgICAgICAgIGNyZWF0ZUFjY291bnQ6IGZhbHNlLFxyXG4gICAgICAgICAgICByZW1lbWJlck1lOiBmYWxzZSxcclxuICAgICAgICAgICAgZm9yZ290UGFzc3dvcmQ6IHRydWUsXHJcbiAgICAgICAgICAgIGxhbmd1YWdlT3B0aW9uOiB0cnVlLFxyXG4gICAgICAgICAgICBzaG93QmFja2dyb3VuZEFuaW1hdGlvbjogdHJ1ZSxcclxuICAgICAgICAgICAgdG9wTG9nbzoge1xyXG4gICAgICAgICAgICAgICAgc3JjOiBcInJlczovL2NsbV9sb2dvXCJcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgYm90dG9tbG9nbzoge1xyXG4gICAgICAgICAgICAgICAgc3JjOiBcIlwiLFxyXG4gICAgICAgICAgICAgICAgdXJsOiBcIlwiLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBjZW50ZXJUZXh0OiBmYWxzZSxcclxuICAgICAgICAgICAgdG9wVGV4dDoge1xyXG4gICAgICAgICAgICAgICAgd2VsY29tZVRleHQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgd2VsY29tZURlc2NyaXB0aW9uOiBcIlwiLFxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBsZWZ0U2lkZUxvZ286IG51bGwsXHJcbiAgICAgICAgICAgIHRlcm1zQ29uZGl0b246IHtcclxuICAgICAgICAgICAgICAgIHVybDE6IFwiXCIsXHJcbiAgICAgICAgICAgICAgICB1cmwyOiBcIlwiLFxyXG4gICAgICAgICAgICAgICAgdXJsMzogXCJcIlxyXG5cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgYWN0aW9uOiBcIlwiLFxyXG4gICAgICAgICAgICBsYW5kaW5nVXJpOiBcIlwiXHJcbiAgICAgICAgfVxyXG5cclxuICAgIGdkcHJDb25maWd1cmF0aW9uIDogYW55ID0ge1xyXG4gICAgICAgIGxvZ286IHtcclxuICAgICAgICAgICAgc3JjOiBcInJlczovL2NsbV9sb2dvXCJcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGxvZ2luU2VydmljZTogTG9naW5TZXJ2aWNlLCBwcml2YXRlIGdkcHJTZXJ2aWNlOiBQbGF0Zm9ybUdkcHJTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgbmF2aWdhdGlvblByb3ZpZGVyOiBOYXZpZ2F0aW9uUHJvdmlkZXIsXHJcbiAgICAgICAgcHJpdmF0ZSB1c2VySW5mb1NlcnZpY2U6IFVzZXJJbmZvU2VydmljZSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdMb2dpbkNvbXBvbmVudCBCaXogQXBwISEnKTtcclxuICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBjb25zb2xlLmxvZygnTG9naW4gUGFnZSBMb2FkZWQnKTtcclxuXHJcbiAgICAgICAgdGhpcy5zdWJzY3JpcHRpb24ucHVzaChcclxuICAgICAgICAgICAgdGhpcy5sb2dpblNlcnZpY2UuZ2RwckNvbnRyb2wuc3Vic2NyaWJlKChlcnJvckRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmKHRoaXMuc2hvd0dEUFIpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNob3dHRFBSID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dCgoKT0+e1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2hvd0dEUFIgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfSwgMSk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXF1aXJlZENvbmRpdGlvbnNEZXRhaWxzID0gZXJyb3JEYXRhO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9uLnB1c2goXHJcbiAgICAgICAgICAgIHRoaXMuZ2RwclNlcnZpY2UuZ2RwckV2ZW50LnN1YnNjcmliZSgocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdkYXRhIHJlc3BvbnNlJywgcmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLmFjY2Vzc190b2tlbil7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRSb3V0ZVNlc3Npb25BZnRlckxvZ2luKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgIH0gICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIHRoaXMuc2hvd0dEUFIgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZXRSb3V0ZVNlc3Npb25BZnRlckxvZ2luKHJlc3BvbnNlOiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCB1c2VyRGV0YWlscyA9IHRoaXMudXNlckluZm9TZXJ2aWNlLmdldFVzZXJEYXRhQnlBY2Nlc3NUb2tlbihyZXNwb25zZS5hY2Nlc3NfdG9rZW4pO1xyXG4gICAgICAgIHRoaXMubmF2aWdhdGlvblByb3ZpZGVyLmFzc2lnbk5hdmlnYXRpb25zVmlzaWJpbGl0eSgpO1xyXG4gICAgICAgIHRoaXMubmF2aWdhdGlvblByb3ZpZGVyLmdldEN1cnJlbnROYXZpZ2F0aW9uT2JzZXJhYmxlKCkuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChkYXRhICYmIGRhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy51c2VySW5mb1NlcnZpY2Uuc2V0VXNlckluZm8odXNlckRldGFpbHMpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy51c2VySW5mb1NlcnZpY2Uuc2VuZFVzZXJJbmZvRXZlbnQodXNlckRldGFpbHMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuc3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc3Vic2NyaXB0aW9uLmZvckVhY2goaXRlbSA9PiBpdGVtLnVuc3Vic2NyaWJlKCkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=