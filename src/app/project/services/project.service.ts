import { Injectable } from '@angular/core';
import { PlatformDataService } from '@ecap3/tns-core';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ProjectService {

    constructor(private platformDataService : PlatformDataService) { 

    }

    getData(pageData) {

        //ItemId,ClientName,Address,Street,StreetNo,City,Zip,CountryName,CountryCode,Latitude,Longitude,CompanyTypes,RegionName,RegionKey,ClientModules,ClientModulesKey,FileId,DisplayEnable,TotalSite,EnableProfile,OrganizationComments>from<Client>where<CompanyTypes=__nin(technical_client)>Orderby<CreateDate __dsc>pageNumber=<0>pageSize= <11>"

        const fields = ['ItemId', 'ClientName', 'Address', 'Street', 'FileId'];
        const query = "Select <"+ fields.join(',') +"> from <Client>where<CompanyTypes=__nin(technical_client)>Orderby<CreateDate __dsc>pageNumber=<"+ pageData.PageNumber +">pageSize= <"+ pageData.PageSize +">"
        return this.platformDataService.getBySqlFilter('Client', query).pipe(map((response: Response) => {
            const parsedData = response.body;
            return parsedData;
        }));
    }
}
