import { Component, OnInit, ViewChild } from '@angular/core';
import { Page } from "tns-core-modules/ui/page";
import { ProjectService } from '../../services/project.service';
import { ActionBarConfiguration } from '@ecap3/tns-core/model/actionBar.interface';
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from 'nativescript-ui-sidedrawer';
import { RadSideDrawerComponent } from 'nativescript-ui-sidedrawer/angular';
import { Router } from '@angular/router';

@Component({
    selector: 'ns-project-default',
    templateUrl: './project-default.component.html',
    styleUrls: ['./project-default.component.css'],
    moduleId: module.id
})
export class ProjectDefaultComponent implements OnInit {
    actionBarConfiguration: ActionBarConfiguration = {
        actionItems: [
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f039')),
                actionItemText: 'Menu 1',
                url: null,
                callParentMethod: false,
                type: 'menu'
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f1e0')),
                actionItemText: 'Menu 2',
                url: null,
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f064')),
                actionItemText: 'Log Out',
                url: null,
                callParentMethod: false,
                type: 'logout'
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f064')),
                actionItemText: 'Go to Project',
                url: '/project',
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f264')),
                actionItemText: 'Menu 5',
                url: null,
                callParentMethod: true
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f263')),
                actionItemText: 'Menu 6',
                url: null,
                callParentMethod: false
            }
        ]
    };
    public data: any;
    public projectList: any = [];
    public totalRecordCount: number;
    public nextDataLoading: boolean;

    public savedVideoSrc: string;
    public pageData: PageData = {
        PageNumber: 0,
        PageSize: 3
    }

    @ViewChild(RadSideDrawerComponent,{static:false}) public drawerComponent: RadSideDrawerComponent;
    private drawer: RadSideDrawer;
    private _sideDrawerTransition: DrawerTransitionBase;

    constructor(private page: Page, 
        private projectService: ProjectService,
        private router: Router) { }

    actionItemClicked(event: any) {
        console.log('project event', event);
    }

    loadOnScroll(event: any) {
        if (this.totalRecordCount > this.projectList.length && !this.nextDataLoading) {
            this.nextDataLoading = true;
            this.pageData.PageNumber += 1;
            //this.getData(event);
        }
    }

    showVideoModal() {
        this.drawer.showDrawer();
    }

    closeModal(){
        this.drawer.closeDrawer();
        /* console.log('flagforuploadbutton',this.flagforuploadbutton);
        if(this.flagforuploadbutton){    
            this.filePath.forEach((filePath, index) => {
                let fileName = 'file'+index;
                this.dashboardService.attachAudioInEvent(fileName,filePath);            
            });
        }; */
    }

    getFiles(){
       /* let files =  this.dashboardService.getFiles();
       this.audioData = files;
       console.log('files',files.length); */
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    received(eventData){
        this.savedVideoSrc = eventData.path
        console.log('savedVideoSrc',this.savedVideoSrc);
        this.drawer.closeDrawer();
    }

    goToImageApp(){
        this.router.navigate(['./project/image']);
    }

    ngOnInit() {
        // console.log(localize("SELISE Smart City"));
        // console.log('dashboard!!!');
        this._sideDrawerTransition = new SlideInOnTopTransition();
    }
    

    ngAfterViewInit() {
        this.drawer = this.drawerComponent.sideDrawer;
    }

}

interface PageData {
    PageNumber: number,
    PageSize: number
}
