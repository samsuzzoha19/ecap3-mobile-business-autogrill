import { Component, OnInit, ViewChild, TemplateRef, Output, EventEmitter } from '@angular/core';
import { ActionBarConfiguration } from '@ecap3/tns-core/model/actionBar.interface';
import { ImagePickerPayload, ImageControlPanel } from '@ecap3/tns-platform-image/types';
import {device, screen} from "tns-core-modules/platform";


@Component({
    selector: 'ns-image-app',
    templateUrl: './image-app.component.html',
    styleUrls: ['./image-app.component.css'],
    moduleId: module.id,
})
export class ImageAppComponent implements OnInit {

    actionBarConfiguration: ActionBarConfiguration = {
        actionItems: [
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f039')),
                actionItemText: 'Menu 1',
                url: null,
                callParentMethod: false,
                type: 'menu'
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f1e0')),
                actionItemText: 'Menu 2',
                url: null,
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f064')),
                actionItemText: 'Log Out',
                url: null,
                callParentMethod: false,
                type: 'logout'
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f064')),
                actionItemText: 'Go to Project',
                url: '/project',
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f264')),
                actionItemText: 'Menu 5',
                url: null,
                callParentMethod: true
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f263')),
                actionItemText: 'Menu 6',
                url: null,
                callParentMethod: false
            }
        ]
    };

    public selectedIndex: number = null;
    isForwardActive: boolean = false;
    isBackwardActive: boolean = false;
    protocolFiles = [];
    public items: Array<string>;

    screenRationWidth:any;
    screenRationHeight:any;
    isTabWidthIsNineSixty:any;


    @ViewChild("filterTemplate",{static:false}) filterTemplate: TemplateRef<any>;
    @ViewChild("imageDetailsDataCapture",{static:false}) imageDetailsCaptureTemplate: TemplateRef<any>;

    // @Input()
    // selectedMinute: Minute = <Minute>{};

    @Output()
    closeParentDrawer: EventEmitter<any> = new EventEmitter();

    @Output()
    onImageSelected: EventEmitter<File> = new EventEmitter();

    imageManagerConfig: ImageControlPanel;

    saveButtonToggle: (status: boolean) => {};

    constructor() { }

    ngOnInit() {
        this.screenRationWidth=(screen.mainScreen.widthDIPs/1280);
        this.screenRationHeight=(screen.mainScreen.heightDIPs/800);
        if (screen.mainScreen.widthDIPs === 960) {
            this.isTabWidthIsNineSixty = true;
        }

        this.isForwardActive = false;
        this.isBackwardActive = false;
        this.selectedIndex = null;
        this.imageManagerConfig = {
            title: "Photo",
            hideGallery: true,
            hideImagePicker: false,
            hideMultiSelectToggle: true,
            hideGalleryFilter: true,
            galleryFilterTemplate: this.filterTemplate,
            imageDetailsCaptureTemplate: this.imageDetailsCaptureTemplate
        };
    }

    
    imageCaptured(data) {
        console.log("event camera Image Captured", data);
    }

    imageUploaded(data) {
        console.log("Image Uploaded", data);
    }

    imageData(data: ImagePickerPayload) {
        console.log("event camera Image Data", data);
    }

    saveAndBackToCamera(imagePath) {
        console.log("saveAndBackToCamera");
        this.storeImage(imagePath);
        this.resetSavePanelStatus();
    }

    saveImage( passData ) {
        // console.log("passData", passData);
        this.storeImage(passData);
        // console.log("saveImage");
        this.closeDrawer();
    }

    private storeImage(passData: any) {
        const imgDirection = this.isForwardActive ? "FORWARD" : "BACKWARD";
        console.log("PassData::" , passData);
        passData.direction = imgDirection;
        // const fileName = `${this.measureInput.ItemId}_${new Date().toISOString()}`;
        // const rootFilePath = File.fromPath(passData.SaveFilePath);
        // const newFile = knownFolders.documents().getFile("images" + path.separator + fileName);
        // const texts = rootFilePath.readSync((e) => console.log("Error", e));
        // console.log("Read Content", texts);
        // newFile.writeSync(texts);
        // console.log("Written Content", written);
        // this.minuteService.attachImageInMinute(newFile, this.selectedMinute, imgDirection).subscribe(data => {
        //     console.log("Inserted Database File", data);
        // });
        // if (!passData.IsSelectFromDevice) {
        //     rootFilePath.remove().then(removed => console.log("Final File Removed", removed));
        // }
        this.onImageSelected.emit(passData);
    }

    listenTemplateEventData(data) {
        console.log("data", data);
        if (data === "FORWARD") {
            this.isForwardActive = true;
            this.isBackwardActive = false;
        } else {
            this.isForwardActive = false;
            this.isBackwardActive = true;
        }
    }

    private resetSavePanelStatus() {
        this.isForwardActive = false;
        this.isBackwardActive = false;
        this.selectedIndex = null;
        this.protocolFiles = [];
        if (this.saveButtonToggle)
            this.saveButtonToggle(false);
    }


    closeDrawer() {
        console.log("ParentCloseDrawerFired");
        this.resetSavePanelStatus();
        this.closeParentDrawer.emit();
    }

    actionItemClicked(event: any) {
        console.log('image event', event);
    }
}
