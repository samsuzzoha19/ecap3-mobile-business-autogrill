import {Injectable} from '@angular/core';
import {AppSettingsProvider} from "@ecap3/tns-core";
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';


@Injectable({
    providedIn: 'root',
})
export class GqlQueryBuilderService {
    header: any = new HttpHeaders({
        'Content-Type': 'application/json'
    });

    constructor(private http: HttpClient,
                private appSettingsProvider: AppSettingsProvider) {
    }

    prepareQuery(entityName: string, fields: any, filters: any, orderBy = '_id: -1', pageNumber = 0, pageSize = 15) {
        let filtersString = '';

        if (Array.isArray(filters)) {
            // search text aka reg search grouped
            const otherFilter = filters.filter(function (filter) {
                return filter.operator !== 'search';
            });
            const groupedFilter = filters.filter(function (filter) {
                return filter.operator === 'search';
            });
            if (Array.isArray(groupedFilter) && groupedFilter.length > 1) {
                const otherFilterQuery = this.prepareFilters(otherFilter);
                const groupedFilterQuery = this.prepareFilters(groupedFilter, ' __or ');
                if (groupedFilterQuery && otherFilterQuery) {
                    filtersString = otherFilterQuery + ' , ( ' + groupedFilterQuery + ' ) ';
                } else if (otherFilterQuery) {
                    filtersString = otherFilterQuery;
                } else {
                    filtersString = groupedFilterQuery;
                }
            } else {
                filtersString = this.prepareFilters(filters);
            }
        } else {
            filtersString = filters ? filters : filtersString;
        }

        return `
            query findData{
                ${entityName}s(
                    Model:{
                        PageNumber: ${pageNumber + 1},
                        Filter: "{ ${filtersString} }",
                        Sort: "{${orderBy}}",
                        PageSize: ${pageSize}
                    }){
                    Data{
                        ${this.queryFieldsMap(fields)}
                    },
                    ErrorMessage,
                    Success,
                    ValidationResult,
                    TotalCount
                }
            }
        `;
    }

    prepareQueryBasic(entityName: string, fields: any, filters: string, orderBy?: any, pageNumber?: number, pageSize?: number) {
        return `
                query findData{
                 ${this.buildQueryBasic({
            entityName, fields, filters, orderBy, pageNumber, pageSize
        })}
                }
              `;
    }

    queryFieldsMap(fields?: Array<string | object>): string {
        return fields
            ? fields
                .map(field =>
                    typeof field === 'object'
                        ? `${Object.keys(field)[0]} { ${this.queryFieldsMap(
                        Object.values(field)[0]
                        )} }`
                        : `${field}`
                )
                .join(', ')
            : '';
    }

    buildQueryBasic(config) {
        return `
            ${config.entityName}s(
            Model:{
             ${config.pageNumber ? `PageNumber: ${config.pageNumber},` : ''}
             Filter: "{ ${config.filters} }"
             ${config.orderBy ? `Sort: ${config.orderBy},` : ''}
             ${config.pageSize ? `PageSize: ${config.pageSize},` : ''}
            }){
              Data{
                ${config.fields.join(',')}
              },
              ErrorMessage,
              Success,
              ValidationResult,
              TotalCount
            }
        `;
    }

    /**
     * Get distinct data
     * @param entityName
     * @param group
     */

    prepareDistinctQuery(entityName: string, group: string) {
        return `
            query distinctQuery {
                AggregateQuery(
                    Model:{
                        EntityName: "${entityName}",
                        Group: "${group}"
                    }){
                     Data,
                     ErrorMessage,
                     Success,
                     ValidationResult,
                     TotalCount
                   }
                }
              `;
    }

    prepareInsertModel(entityName: string, model: any) {
        return `
            mutation create{
                ${this.buildInsertQuery({entityName, model})}
            }
        `;
    }

    buildInsertQuery(config) {
        const modelString = this.convertObjectToGQLString(config.model);
        return `
            ${config.entityName}(
                Model: ${modelString}, MutationType: INSERT
            ) {
            ErrorMessage
            Result
            Success
            ValidationResult
            }
        `;
    }

    prepareUpdateModel(entityName: string, model: any, entityId: string) {
        // Remove `ItemId` from Model when GraphQL update
        if (model['ItemId']) {
            delete model['ItemId'];
        }

        return `
            mutation update{
                ${this.buildUpdateQuery({entityName, model, entityId})}
            }
        `;
    }

    buildUpdateQuery(config) {
        const modelString = this.convertObjectToGQLString(config.model);
        return `
            ${config.entityName}(
                Model: ${modelString},
                MutationType: UPDATE,
                Filter: "{_id:'${config.entityId}'}"
            ) {
            ErrorMessage
            Result
            Success
            ValidationResult
            }
        `;
    }

    /**
     * Insert Item in array in multilevel
     * @param entityName
     * @param model
     * @param entityId
     * @param filter
     * @param pushModal
     * @param setProp
     * @param ArrayFilter  // optional
     */
    prepareInsertNestedArrayModel(entityName: string, model: any, entityId: string, filter: string, pushModal: any, setProp: string, ArrayFilter?: string) {

        return `
            mutation insertNested {
                ${this.buildNestedInsertQuery({
            entityName, model, entityId, filter, pushModal, setProp, ArrayFilter
        })}
            }
        `;
    }

    buildNestedInsertQuery(config) {
        const modelString = this.convertObjectToGQLString(config.model);
        const pushQuery = this.convertObjectToGQLStringForUpdate(config.pushModal, '');
        const arrayFilter = config.ArrayFilter ? `, ArrayFilter: "{${config.ArrayFilter}}"` : '';
        return `
            ${config.entityName}(
                Model: ${modelString},
                MutationType: PushUpdate,
                Filter: "{${config.entityId ? `_id:'${config.entityId}'` : ''} ${config.filter ? `${(config.entityId ? ', ' : '') + config.filter}` : ''}}",
                PushQuery: "{${config.setProp}: ${pushQuery}}",
                ${arrayFilter}
            ) {
            ErrorMessage
            Result
            Success
            ValidationResult
            }
        `;
    }

    /**
     * Update Item in array in multilevel
     * @param entityName
     * @param model
     * @param entityId
     * @param filter
     * @param pushModal
     * @param setProp
     * @param ArrayFilter
     */

    prepareNestedArrayUpdateModel(entityName: string, model: any, entityId: string, filter: string, pushModal: any, setProp: string, ArrayFilter: string) {
        return `
            mutation updateNested {
                ${this.buildNestedUpdateQuery({
            entityName, model, entityId, filter, pushModal, setProp, ArrayFilter
        })}
            }
        `;
    }

    buildNestedUpdateQuery(config) {
        const modelString = this.convertObjectToGQLString(config.model);
        const updateQuery = this.convertObjectToGQLStringForUpdate(config.pushModal, `${config.setProp}`);
        const arrayFilter = config.ArrayFilter ? `, ArrayFilter: "{arrayFilter:[${config.ArrayFilter}]}"` : '';
        return `
            ${config.entityName}(
                Model: ${modelString},
                MutationType: PushUpdate,
                Filter: "{${config.entityId ? `_id:'${config.entityId}'` : ''} ${config.filter ? `${(config.entityId ? ', ' : '') + config.filter}` : ''}}",
                UpdateQuery: "${updateQuery}"
                ${arrayFilter}
            ) {
            ErrorMessage
            Result
            Success
            ValidationResult
            }
        `;
    }

    /**
     * Update Item in Object in multilevel
     * @param entityName
     * @param model
     * @param entityId
     * @param filter
     * @param pushModal
     * @param setProp
     */
    prepareNestedObjectUpdateModel(entityName: string, model: any, entityId: string, filter: string, pushModal: any, setProp: string) {
        return `
            mutation updateNested{
                ${this.buildNestedObjectUpdate({
            entityName, model, entityId, filter, pushModal, setProp
        })}
            }
        `;
    }

    buildNestedObjectUpdate(config) {
        const modelString = this.convertObjectToGQLString(config.model);
        const updateQuery = this.convertObjectToGQLStringForUpdate(config.pushModal, `${config.setProp}`);
        return `
            ${config.entityName}(
                Model: ${modelString},
                MutationType: PushUpdate,
                Filter: "{${config.entityId ? `_id:'${config.entityId}'` : ''} ${config.filter ? `${(config.entityId ? ', ' : '') + config.filter}` : ''}}",
                UpdateQuery: "${updateQuery}"
            ) {
            ErrorMessage
            Result
            Success
            ValidationResult
            }
        `;
    }

    /**
     * remove Item in array in multilevel
     * @param entityName
     * @param model
     * @param entityId
     * @param filter
     * @param updateQuery
     * @param ArrayFilter // optional
     */
    prepareNestedArrayItemRemoveModel(entityName: string, model: any, entityId: string, filter: string, updateQuery: any, ArrayFilter?: string) {

        return `
            mutation pullUpdate{
                ${this.buildQueryForArrayItemRemoveModel({
            entityName, model, entityId, filter, updateQuery, ArrayFilter
        })}
            }
        `;
    }

    buildQueryForArrayItemRemoveModel(config) {
        const modelString = this.convertObjectToGQLString(config.model);
        const arrayFilter = config.ArrayFilter ? `, ArrayFilter: "{${config.ArrayFilter}}"` : '';
        return `
            ${config.entityName}(
            Model: ${modelString},
            MutationType: PULL_UPDATE,
            Filter: "{${config.entityId ? `_id:'${config.entityId}'` : ''} ${config.filter ? `${(config.entityId ? ', ' : '') + config.filter}` : ''}}",
            UpdateQuery: "{${config.updateQuery}}"
            ${arrayFilter}
         ) {
           ErrorMessage
           Result
           Success
           ValidationResult
         }
        `;
    }

    /**
     *
     * @param entityName
     * @param entityId
     */
    prepareDeleteModel(entityName: string, entityId: string) {
        return `
            mutation delete{
             ${this.buildQueryForDelete({entityName, entityId})}
            }
        `;
    }

    buildQueryForDelete(config) {
        return `
        ${config.entityName}(
            MutationType: DELETE,
            Filter: "{_id:'${config.entityId}'}"
         ) {
           ErrorMessage
           Result
           Success
           ValidationResult
         }`;
    }

    aggregateMultipleCall(query: any[]) {
        return `
            mutation aggregateCall{
             ${query.join('  ')}
            }
        `;
    }

    /**
     * For Now No need this function
     *
     */
    aggregateMutationCreateModel(entityName: string, model: any) {
        const modelString = this.convertObjectToGQLString(model);
        const mutationQuery = `mutation insert{
             ${entityName}(
                Model: ${modelString},
                MutationType: INSERT
             ) {
               ErrorMessage
               Result
               Success
               ValidationResult
             }
            }
        `;

        return {
            'Uri': this.appSettingsProvider.getAppSettings().GQLMutationUri,
            'Verb': 'Post',
            'Payload': JSON.stringify({
                'query': mutationQuery,
                'operationName': 'insert',
                'variables': {}
            }),
            'SuccessIf': '{\"data.' + entityName + '.Success\":\"True\"}'
        };
    }

    /**
     * For Now No need this function
     *
     */
    aggregateMutationUpdateModel(entityName: string, model: any) {
        if (model.ItemId) {
            const itemId = model.ItemId;
            delete model.ItemId; // remove `ItemId` field from model for update call

            const modelString = this.convertObjectToGQLString(model);
            const mutationQuery = `mutation update{
             ${entityName}(
                Model: ${modelString},
                MutationType: UPDATE,
                Filter: "{_id:'${itemId}'}"
             ) {
               ErrorMessage
               Result
               Success
               ValidationResult
             }
            }
        `;

            return {
                'Uri': this.appSettingsProvider.getAppSettings().GQLMutationUri,
                'Verb': 'Post',
                'Payload': JSON.stringify({
                    'operationName': 'update',
                    'variables': {},
                    'query': mutationQuery,
                }),
                'SuccessIf': '{"data.' + entityName + '.Success":"True"}'
            };
        }
    }

    prepareFilters(filters: any, expressionOperator = null) {
        let preparedFilters = '';
        if (!expressionOperator) {
            expressionOperator = ' , ';
        }
        for (let i = 0, j = 0; i < filters.length; i++) {
            if (!this.isInvalid(filters[i].value)) {
                if (Array.isArray(filters[i].value)) {
                    if (filters[i].value.length < 1) {
                        continue;
                    }
                }
                j++;
                if (j > 1) {
                    preparedFilters += expressionOperator;
                }
                const expression = this.prepareExpression(filters[i]);
                preparedFilters += expression;

            }

        }

        return preparedFilters;
    }

    prepareExpression(expressionObject: any) {
        let preparedExpression = '';
        const operator = expressionObject.operator;
        const property = expressionObject.property;
        const value = expressionObject.value;

        const convertedOperator = {
            '=': `'${value}'`,
            'search': `/${expressionObject.value}/i`,
            'raw': `'${value}'`, // Support for raw mongo filter
            // '|': '__or',
            // '>': '__gt',
            // '>=': '__gte',
            // '&': '__and',
            // '<': '__lt',
            // '<=': '__lte',
            // '!=': '__ne',
            // 'range': '__inc',
            // 'contains': '__in',
            // '!contains': '__nin',
        };

        if (operator !== 'raw') {
            preparedExpression += property;
            preparedExpression += ':';
            preparedExpression += convertedOperator[operator];
        } else {
            preparedExpression = value;
        }

        return preparedExpression;
    }

    isInvalid(value: any) {
        return value === undefined || value === null || value === '';
    }

    /**
     * @desc Converts nested Object to GraphQL string (Work In Progress)
     * @author Shaiful Bappy
     * @param fields
     * @param isRecursive
     */

    convertObjectToGQLString(fields, isRecursive = false) {
        let gqlString = '';
        const tempArray = [];
        if (fields) {
            Object.keys(fields).map(key => {
                if (Array.isArray(fields[key])) {
                    // If value type is `Array`, convert the child value recursively
                    tempArray.push(
                        isNaN(parseInt(key, 10)) ? `${key}: [${this.convertObjectToGQLString(fields[key], true)}]` : `[${this.convertObjectToGQLString(fields[key], true)}]`
                    );
                } else if ((typeof fields[key] === 'object' && fields[key] !== null)) {
                    // If value type is `Object`, convert the child value recursively
                    tempArray.push(
                        isNaN(parseInt(key, 10)) ? `${key}: {${this.convertObjectToGQLString(fields[key], true)}}` : `{${this.convertObjectToGQLString(fields[key], true)}}`
                    );

                } else if ((typeof fields[key] === 'boolean') || (typeof fields[key] === 'number')) {
                    // If value type is `Boolean`, do not enclose the value with quote
                    tempArray.push(
                        isNaN(parseInt(key, 10)) ? `${key}: ${fields[key]}` : `${fields[key]}`
                    );
                } else {
                    // If value type is `String` or `Other`, enclose the value with quote
                    if (fields[key] === null) {
                        tempArray.push(
                            isNaN(parseInt(key, 10)) ? `${key}: ${fields[key]}` : `${fields[key]}`
                        );
                    } else {
                        if (fields[key] !== undefined) {
                            // Encode double and single quote characters
                            let stringValue = fields[key];
                            stringValue = stringValue.replace(/"/g, '\\"');
                            stringValue = stringValue.replace(/'/g, '\'');

                            // encode `newline`
                            stringValue = stringValue.replace(/(?:\r\n|\r|\n)/g, '<br>');

                            tempArray.push(
                                isNaN(parseInt(key, 10)) ? `${key}: "${stringValue}"` : `"${stringValue}"`
                            );
                        }
                    }
                }
            });

            gqlString = `{${tempArray.join(',')}}`;
        }

        return isRecursive ? tempArray : gqlString;

        /* Alternative solution */
        // const gqlString = JSON.stringify(fields);
        // const gqlString = gqlString.replace(/"([^(")"]+)":/g,"$1:");
        // return gqlString;
    }

    convertObjectToGQLStringForUpdate(fields, propName: string, isRecursive = false) {
        let gqlString = '';
        const tempArray = [];
        if (fields) {
            Object.keys(fields).map(key => {
                if (Array.isArray(fields[key])) {
                    // If value type is `Array`, convert the child value recursively
                    tempArray.push(
                        isNaN(parseInt(key, 10)) ? `'${propName}${key}': [${this.convertObjectToGQLStringForUpdate(fields[key], '', true)}]` : `[${this.convertObjectToGQLStringForUpdate(fields[key], '', true)}]`
                    );
                } else if ((typeof fields[key] === 'object' && fields[key] !== null)) {
                    // If value type is `Object`, convert the child value recursively
                    tempArray.push(
                        isNaN(parseInt(key, 10)) ? `'${propName}${key}': {${this.convertObjectToGQLStringForUpdate(fields[key], '', true)}}` : `{${this.convertObjectToGQLStringForUpdate(fields[key], '', true)}}`
                    );

                } else if ((typeof fields[key] === 'boolean') || (typeof fields[key] === 'number')) {
                    // If value type is `Boolean`, do not enclose the value with quote
                    tempArray.push(
                        isNaN(parseInt(key, 10)) ? `'${propName}${key}': ${fields[key]}` : `${fields[key]}`
                    );
                } else {
                    // If value type is `String` or `Other`, enclose the value with quote
                    if (fields[key] === null) {
                        tempArray.push(
                            isNaN(parseInt(key, 10)) ? `'${propName}${key}': ${fields[key]}` : `${fields[key]}`
                        );
                    } else {
                        // Encode double and single quote characters
                        let stringValue = fields[key];
                        stringValue = stringValue.replace(/"/g, '\\"');
                        stringValue = stringValue.replace(/'/g, '\'');

                        // encode `newline`
                        stringValue = stringValue.replace(/(?:\r\n|\r|\n)/g, '<br>');

                        tempArray.push(
                            isNaN(parseInt(key, 10)) ? `'${propName}${key}': '${stringValue}'` : `'${stringValue}'`
                        );
                    }
                }
            });

            gqlString = `{${tempArray.join(',')}}`;
        }

        return isRecursive ? tempArray : gqlString;

    }

    getQuery(queryString): Observable<any> {
        const payload = {
            operationName: 'findData',
            variables: {},
            query: queryString
        };
        return this.http.post(this.appSettingsProvider.getAppSettings().GQLQueryUri,
            payload,
            {headers: this.header, withCredentials: true, observe: 'response'})
            .pipe(map((response) => response.body));
    }

    mutateQuery(mutateString, type: 'create' | 'update' = 'create'): Observable<any> {
        const payload = {
            operationName: type,
            variables: {},
            query: mutateString
        };
        return this.http.post(this.appSettingsProvider.getAppSettings().GQLMutationUri,
            payload,
            {headers: this.header, withCredentials: true, observe: 'response'})
            .pipe(map((response) => response.body));
    }

    private IsJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }
}
