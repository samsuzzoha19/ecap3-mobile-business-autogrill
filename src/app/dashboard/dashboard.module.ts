import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DefaultPageComponent } from "./components/default-page/default-page.component";
import { CoreTemplateModule } from "@ecap3/tns-core-template";
import { NativeScriptLocalizeModule } from "nativescript-localize/angular";
import { WebViewExtModule } from "@nota/nativescript-webview-ext/angular";
import { PlatformAudioModule } from "@ecap3/tns-platform-audio";
import { ModalContentComponent } from "@ecap3/tns-platform-dialog/components/modal-content/modal-content.component";
import { PlatformDialogModule } from "@ecap3/tns-platform-dialog";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        DashboardRoutingModule,
        CoreTemplateModule,
        NativeScriptLocalizeModule,
        WebViewExtModule,
        PlatformAudioModule,
        PlatformDialogModule,
        NativeScriptUISideDrawerModule
    ],
    declarations: [
        DefaultPageComponent,
       // ModalContentComponent
    ],
    entryComponents: [
        //ModalContentComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class DashboardModule { }
