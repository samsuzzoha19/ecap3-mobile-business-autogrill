import { Injectable } from '@angular/core';
import { LocalPlatformDataService } from '@ecap3/tns-core';
import { QueryWhereItem, Query, QueryLogicalOperator } from 'nativescript-couchbase-plugin';
import {Observable, of} from "rxjs";
import {catchError, map} from "rxjs/operators";
import {GqlQueryBuilderService} from "~/app/shared-data/services/gql-query-builder.service";

@Injectable({
    providedIn: 'root'
})
export class DashboardService {

    constructor(public localPlatformDataService: LocalPlatformDataService,
                private gqlQueryBuilder: GqlQueryBuilderService,) { }

    attachAudioInEvent(fileName, filePath) {
        const payload = {
            ItemId: this.localPlatformDataService.getGuid(),
            Name: fileName,
            EntityName: "File",
            MediaType: "Audio",
            LocalPath: filePath,
            IsFileUploaded: false,
            IsFileDownloaded: true,
            Tags: ['Is-Valid-Audio'],
            MetaData: {
                FileInfo: {
                    Type: "Value",
                    Value: fileName,
                },
                MediaType: {
                    Type: "MediaType",
                    Value: "Audio"
                }
            }
        };
         console.log("FilePayload::", payload);
        return this.localPlatformDataService.insert(payload);
    }

    getFiles() {
        const filter : QueryWhereItem[] = [
            {
               property: 'EntityName',
               comparison: 'equalTo',
               value: "File"
           }/* ,
           {
               logical: QueryLogicalOperator.AND,
               property: 'FilterType',
               comparison: 'equalTo',
               value: filterType,
           } */
       ];

       const query: Query = {
           select: [],
           where: filter
       };
       return this.localPlatformDataService.getBySqlFilter(query);
    }

    getPersonByEmail(email: string): Observable<any> {
        // Test Graphql Code
        const entityName = 'Person';
        const selectFields =['ItemId',
            'Tags',
            'DateOfBirth',
            'FirstName',
            'LastName',
            'MiddleName',
            'DisplayName',
            'ProfileImageId',
            'Email',
            'Salutation'
        ];
        const filters = [
            {
                property: 'Email',
                operator: '=',
                value: 'wk@selise.ch'
            }
        ];

        const queryString = this.gqlQueryBuilder.prepareQuery(entityName, selectFields, filters);
        return this.gqlQueryBuilder.getQuery(queryString).pipe(
            map(response => {
            console.log(response);
                if (response.data && response.data[entityName + 's']) {
                    return response.data[entityName + 's'].Data && response.data[entityName + 's'].Data.length > 0 ?
                        response.data[entityName + 's'].Data[0] : null;
                }

                return of(null);
            })
        );
    }
}
