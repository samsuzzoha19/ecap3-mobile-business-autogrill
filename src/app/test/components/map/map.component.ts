import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActionBarConfiguration } from '@ecap3/tns-core/model/actionBar.interface';
import { MapView } from 'nativescript-google-maps-sdk';
import {registerElement} from "nativescript-angular/element-registry";

// Important - must register MapView plugin in order to use in Angular templates
registerElement("MapView", () => require("nativescript-google-maps-sdk").MapView);

@Component({
  selector: 'ns-map',
//   templateUrl: './map.component.html',
//   styleUrls: ['./map.component.css'],
template: `
    <GridLayout>
        <MapView (mapReady)="onMapReady($event)"></MapView>
    </GridLayout>
    `,
  moduleId: module.id,
})
export class MapComponent implements OnInit {
    @ViewChild("MapView", {static: true}) mapView: ElementRef;
    actionBarConfiguration: ActionBarConfiguration = {
        actionItems: [
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f039')),
                actionItemText: 'Menu 1',
                url: null,
                callParentMethod: false,
                type: 'menu'
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f1e0')),
                actionItemText: 'Menu 2',
                url: null,
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f064')),
                actionItemText: 'Log Out',
                url: null,
                callParentMethod: false,
                type: 'logout'
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f064')),
                actionItemText: 'Go to Project',
                url: '/project',
                callParentMethod: false
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f264')),
                actionItemText: 'Menu 5',
                url: null,
                callParentMethod: true
            },
            {
                actionItemIcon: String.fromCharCode(parseInt('0x' + 'f263')),
                actionItemText: 'Menu 6',
                url: null,
                callParentMethod: false
            }
        ]
    };
  constructor() { }

  ngOnInit() {
  }

  onMapReady = (event) => {
    let mapView = event.object as MapView;
    const NA_CENTER_LATITUDE = 39.8283459;
    const NA_CENTER_LONGITUDE = -98.5816737;

    mapView.latitude = NA_CENTER_LATITUDE;
    mapView.longitude = NA_CENTER_LONGITUDE;
    mapView.zoom = 3;
}
  actionItemClicked(event){}
}
