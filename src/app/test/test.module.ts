import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { MapComponent } from './components/map/map.component';
import { TestRoutingModule } from './test-routing.module';


@NgModule({
  declarations: [MapComponent],
  imports: [
    NativeScriptCommonModule,
    TestRoutingModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class TestModule { }
