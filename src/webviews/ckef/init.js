﻿/**
 * Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/* exported initSample */

if ( CKEDITOR.env.ie && CKEDITOR.env.version < 9 )
	CKEDITOR.tools.enableHtml5Elements( document );

// The trick to keep the editor in the sample quite small
// unless user specified own height.
CKEDITOR.config.height = '480';
CKEDITOR.config.width = 'auto';

var initEditor = ( function() {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

	return function() {
		var editorElement = CKEDITOR.document.getById( 'editor' );

		// :(((
		if ( isBBCodeBuiltIn ) {
			editorElement.setHtml();
		}

		// Depending on the wysiwygarea plugin availability initialize classic or inline editor.
		if ( wysiwygareaAvailable ) {
			var ckvEditor = CKEDITOR.replace( 'editor' );
            // CKEDITOR.replace("editor", {
             //    on : {
             //        // maximize the editor on startup
             //        'instanceReady' : function( evt ) {
             //            console.log("Editor");
             //            evt.editor.resize("100%", document.getElementById('area').style.height);
             //        }
             //    }
			// });
			CKEDITOR.instances.editor.on('change', function() {
                // console.log("Change");
				callFromNativeScript();
			});
            // CKEDITOR.instances.editor.on('instanceReady', function(evt) {
            //     evt.editor.resize("100%", document.getElementById('area').style.height);
            // });
		} else {
			editorElement.setAttribute( 'contenteditable', 'true' );
			CKEDITOR.inline( 'editor' );
            // console.log("editor wysiwygareaAvailable");
			// TODO we can consider displaying some info box that
			// without wysiwygarea the classic editor may not work.
		}
	};

	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}

		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
    function callFromNativeScript() {
        const editorData = CKEDITOR.instances.editor.getData();
        //console.log('got a message from nativescript');
        window.nsWebViewBridge.emit('gotMessage', {huba: editorData});
    }
} )();

function registerMinuteData( minuteData ) {
	console.log("Minute data", minuteData);
	window.nsWebViewBridge.on(minuteData, function (data) {
		CKEDITOR.instances.editor.setData( data);
	})
}