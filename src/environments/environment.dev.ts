import {microServiceVersions} from './micro.service.version';

const tenantId = `A54D6D82-61F6-4BD0-BAF8-BD06B0AD72FF`;
const domain = `seliselocal.com`;
const subDomain = `delta`;
const microserviceDomain = `delta`;
const origin = `http://${subDomain}.${domain}`;
const microserviceUrl = `http://${microserviceDomain}.${domain}/api/`;

export const environment = {
    production: false,
    IsStaging: false,
    isSSo: false,
    LocalDBName: "delta-db",
    Apps: `${microserviceUrl}appcatalogue/${microServiceVersions.appcatalogue.dev}AppCatalogue/AppManagerQuery/GetApps`,
    Token: `${microserviceUrl}identity/${microServiceVersions.identity.dev}identity/token`,
    SecurityIdentity: `${microserviceUrl}identity/${microServiceVersions.identity.dev}identity/`,
    Captcha: `${microserviceUrl}captcha/${microServiceVersions.captcha.dev}Captcha/CaptchaCommand/`,
    Identity: `${microserviceUrl}tenant/${microServiceVersions.tenant.dev}TenantManager/TenantManagerQuery/GetIdentity`,
    Navigations: `${microserviceUrl}tenant/${microServiceVersions.tenant.dev}TenantManager/SiteManagerQuery/GetNavigations`,
    TenantId: tenantId,
    Origin: origin,
    Authentication: `${microserviceUrl}identity/${microServiceVersions.identity.dev}identity/Authentication/`,
    Sites: `${microserviceUrl}tenant/${microServiceVersions.tenant.dev}TenantManager/SiteManagerQuery/GetSites`,
    Security: `${microserviceUrl}uam/${microServiceVersions.uam.dev}UserAccessManagement/`,
    DataCore: `${microserviceUrl}pds/AppSuiteDataCore/`,
    Storage: `${microserviceUrl}storageservice/${microServiceVersions.storageservice.dev}StorageService/`,
    EmailService: `${microserviceUrl}mailservice/${microServiceVersions.mailservice.dev}MailService`,
    SubDomainName: `${subDomain}.${domain}`,
    NotificationService: `${microserviceUrl}notification/${microServiceVersions.notification.dev}`,
    AggregateService: `${microserviceUrl}aggregator/${microServiceVersions.aggregator.dev}Service-Aggregator/ServiceAggrigation/`,
    DocumentManagerService: `${microserviceUrl}dms/${microServiceVersions.dms.dev}/DmsService/`,
    CalendarApiService: `${microserviceUrl}calendarservice/${microServiceVersions.calendarservice.dev}CalendarService/`,
    GQLQueryUri: `${microserviceUrl}gqlquery/${microServiceVersions.graphQlQuery.dev}graphql`,
    GQLMutationUri: `${microserviceUrl}gqlcommand/${microServiceVersions.graphQlCommand.dev}graphql`,
    UilmService: `${microserviceUrl}uilm/languagemanager`,
    KiwiBusinessService: `${microserviceUrl}business-kiwi/KiwiBusinessService/`,
    DefaultPackagePath: './assets/i18n/generic-app/',
    CdnBaseUrl: 'https://cdn.selise.biz/hsap/',
    SiteLogo1: 'https://cdn.selise.biz/vorwerk/Vorwerk_Logo_800px.png',
    SiteLogo2: 'https://cdn.selise.biz/vorwerk/ThermomixLogo.svg',
    SiteLogoComb: 'https://cdn.selise.biz/vorwerk/Vorwerk_and_Thermomix_logo.svg',
    StorageBaseUrl: 'http://cdn.selise.biz/',
    CdnTenantBaseUrl: 'https://cdn.selise.biz/',
    StaticResourceCDN: 'https://stg-ecap.s3.eu-central-1.amazonaws.com/',
    action: `${origin}/auth`,
    landingUri: origin,
    Client_ID: tenantId,
    DomainName: `${domain}`,
    SessionKey: `set_from_${subDomain}.${domain}`,
    TransportMedium: ['webSockets', 'foreverFrame', 'serverSentEvents', 'longPolling'],
    NotificationHubName: 'NotifierServerHub',
    PersonFields: ['ItemId', 'CreatedBy', 'Tags', 'Roles', 'DateOfBirth', 'Interests', 'ShortBio', 'Sex', 'FirstName', 'LastName', 'MiddleName',
        'ProfileImage', 'Email', 'DisplayName', 'Countires', 'ProfileImageId', 'Currency', 'IsVerified', 'Skills', 'Salutation', 'CompanyName',
        'AddressLine1', 'AddressLine2', 'Street', 'PostalCode', 'City', 'Country', 'State', 'Nationality', 'PhoneNumber', 'AccountNumber',
        'Designation', 'Organization', 'OrganizationId', 'LastLogInTime', 'LogInCount', 'OfficialPhoneNumber', 'OfficeCountryCode', 'MarketName',
        'Department', 'ContactType', 'SecondaryEmails', 'SecondaryPhoneNumbers', 'SecondaryPhoneNumbersPlainText', 'ContactEmails',
        'ContactPhoneNumbers', 'ColorCode', 'Location', 'LocationCoordinates', 'CustomerRole', 'CustomerId', 'EmployeeID', 'Language',
        'Canton', 'CreateDate', 'Preferences', 'EmbededInfo', 'Active', 'LanguageSpeakes', 'CookingForPersonNumber', 'CookingTimesPerWeek',
        'CookingImportance', 'CookingBothered', 'CookingBotheredOthers', 'LastUpdatedBy', 'KiwiId', 'CurrentAddress'],
    language: [
        {id: 'en', title: 'English', flag: 'us', value: 'en-US'},
        {id: 'de', title: 'German', flag: 'de', value: 'de-DE'},
        {id: 'fr', title: 'French', flag: 'fr', value: 'fr-FR'},
        {id: 'it', title: 'Italian', flag: 'it', value: 'it-IT'}
    ]
};
