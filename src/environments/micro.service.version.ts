export const microServiceVersions = {
    captcha: {
        dev: 'v1/',
        stg: 'v1/',
        prod: 'v1/'
    },
    tenant: {
        dev: 'v1/',
        stg: 'v1/',
        prod: 'v1/'
    },
    messagingservice: {
        dev: 'v22/',
        stg: 'v22/',
        prod: 'v22/'
    },
    mailservice: {
        dev: 'v6/',
        stg: 'v6/',
        prod: 'v6/'
    },
    storageservice: {
        dev: 'v1/',
        stg: 'v1/',
        prod: 'v1/'
    },
    securestorage: {
        dev: 'v5/',
        stg: 'v5/',
        prod: '/'
    },
    shortmessage: {
        dev: 'v11/',
        stg: 'v11/',
        prod: '/'
    },
    notification: {
        dev: 'v1',
        stg: 'v1',
        prod: '1'
    },
    appcatalogue: {
        dev: 'v1/',
        stg: 'v1/',
        prod: 'v1/'
    },
    sequencenumber: {
        dev: 'v12/',
        stg: 'v12/',
    },
    aggregator: {
        dev: 'v3/',
        stg: 'v3/',
        prod: 'v3/'
    },
    dictionaryservice: {
        dev: 'v4/',
        stg: 'v4/',
        prod: 'v1/'
    },
    pdfgenerator: {
        dev: 'v40/',
        stg: 'v40/',
        prod: 'v5/'
    },
    identity: {
        dev: 'v7/',
        stg: 'v7/',
        prod: 'v7/'
    },
    uam: {
        dev: 'v10/',
        stg: 'v10/',
        prod: 'v10/'
    },
    pds: {
        dev: 'v1/',
        stg: 'v1/',
        prod: 'v1/',
    },
    graphQlCommand: {
        dev: 'v3/',
        stg: 'v1/',
        prod: 'v3/'
    },
    graphQlQuery: {
        dev: 'v3/',
        stg: 'v1/',
        prod: 'v3/'
    },
    calendarservice: {
        dev: 'v1/',
        stg: 'v1/',
        prod: 'v1/'
    },
    dms: {
        dev: 'v1/',
        stg: 'v1/',
        prod: 'v1/'
    }
};
