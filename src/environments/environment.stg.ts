import { microServiceVersions } from "./micro.service.version";

export const environment = {
    isMock: false,
    production: false,
    TenantId: "8DB8BDF2-5222-4E45-96CC-593CDD815D88",
    Client_ID: "8DB8BDF2-5222-4E45-96CC-593CDD815D88",
    DomainName: "selise.biz",
    SubDomainName: "https://stg-trackord.selise.biz",
    Origin: "https://stg-trackord.selise.biz",

    Token: "https://stg-security.selise.biz/security/token",
    Security: "https://stg-security.selise.biz/Security/",
    Authentication: "https://stg-security.selise.biz/Security/Authentication/",

    DataCore: "https://stg-pds.selise.biz/AppSuiteDataCore/",

    StorageBaseUrl: "http://cdn.selise.biz/",
    StorageFlagUrl: "common/flag/icon/",
    StorageImageUrl: "grandbasel/assets/",
    CdnTenantBaseUrl: "https://cdn.selise.biz/slnetwork/",
    StaticResourceCDN: "https://stg-ecap.s3.eu-central-1.amazonaws.com/",
    SecureStorage: "http://securestorage.fakedomain.com/StorageService/",
    Storage: `https://stg-storage.selise.biz/${microServiceVersions.storageservice.stg}StorageService/`,

    Apps: `https://stg-catalogue.selise.biz/${microServiceVersions.appcatalogue.stg}AppCatalogue/AppManagerQuery/GetApps`,

    Navigations: `https://stg-tenant.selise.biz/${microServiceVersions.tenant.stg}TenantManager/SiteManagerQuery/GetNavigations`,
    Identity: `https://stg-tenant.selise.biz/${microServiceVersions.tenant.stg}TenantManager/TenantManagerQuery/GetIdentity`,
    Sites: `https://stg-tenant.selise.biz/${microServiceVersions.tenant.stg}TenantManager/SiteManagerQuery/GetSites`,

    EmailService: `https://stg-mail.selise.biz/${microServiceVersions.mailservice.stg}MailService`,
    MessaginService: `https://stg-MessagingService.selise.biz/${microServiceVersions.messagingservice.stg}MessagingService/`,

    AggregateService: `https://stg-aggregate.selise.biz/${microServiceVersions.aggregator.stg}Service-Aggregator/ServiceAggrigation/`,

    NotificationHubName: "NotifierServerHub",
    NotificationService: `https://stg-notification.selise.biz/${microServiceVersions.notification.stg}`,

    DictionaryService: `https://stg-dictionaryservice.selise.biz/${microServiceVersions.dictionaryservice.stg}DictionaryService/`,

    PdfGeneratorService: `https://stg-pdfgenerator.selise.biz/${microServiceVersions.pdfgenerator.dev}PdfGeneratorService/`,
    TemplateEngineService: "https://stg-templateengine.selise.biz/TemplateEngineService/",

    PersonFields: ["ItemId", "CreatedBy", "Tags", "DateOfBirth", "Interests", "ShortBio", "Sex",
        "FirstName", "LastName", "MiddleName", "ProfileImage", "Email", "DisplayName", "Countires",
        "ProfileImageId", "Currency", "IsVerified", "Skills", "Salutation", "CompanyName",
        "AddressLine1", "CustomerId", "AddressLine2", "Street", "PostalCode", "City", "Country", "State", "Nationality",
        "PhoneNumber", "AccountNumber", "Designation", "Organization", "LastLogInTime", "LogInCount", "OfficialPhoneNumber",
        "OfficeCountryCode", "MarketName", "ColorCode", "ContactEmails", "ContactPhoneNumbers", "ManagerNr"]
};