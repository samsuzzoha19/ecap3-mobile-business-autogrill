import { INavigationItem, NavigationType } from "@ecap3/tns-core/model/navigation.interface";

export const navigations : INavigationItem[] = [
    {
        'id': 'dashboard',
        'title': 'Dashboard',
        'translate': 'DASHBOARD',
        'type': NavigationType.ITEM,
        'url': '/dashboard',
        'icon': String.fromCharCode(parseInt('0x'+'f042')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false,
        'order': 1
    },
    {
        'id': 'project',
        'title': 'Project',
        'translate': 'PROJECT',
        'type': NavigationType.ITEM,
        'url': '/project',
        'icon': String.fromCharCode(parseInt('0x'+'f045')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false,
        'order': 2
    },
    {
        'id': 'project',
        'title': 'Audit',
        'translate': 'PROJECT',
        'type': NavigationType.ITEM,
        'url': '',
        'icon': String.fromCharCode(parseInt('0x'+'f046')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false,
        'order': 3
    },
    {
        'id': 'project',
        'title': 'Management',
        'translate': 'PROJECT',
        'type': NavigationType.ITEM,
        'url': '',
        'icon': String.fromCharCode(parseInt('0x'+'f1a5')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false,
        'order': 4
    },
    {
        'id': 'project',
        'title': 'Users',
        'translate': 'PROJECT',
        'type': NavigationType.ITEM,
        'url': '',
        'icon': String.fromCharCode(parseInt('0x'+'f19c')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false,
        'order': 5
    },
    {
        'id': 'settings',
        'title': 'Settings',
        'translate': NavigationType.ITEM,
        'type': 'item',
        'url': '',
        'icon': String.fromCharCode(parseInt('0x'+'f013')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false,
        'order': 6
    },
    {
        'id': 'test',
        'title': 'TEST',
        'translate': NavigationType.ITEM,
        'type': 'item',
        'url': '/test',
        'icon': String.fromCharCode(parseInt('0x'+'f013')),
        'iconType': 'SLIcon',
        'isVisible': true,
        'isHidden': false,
        'order': 7
    }
];
