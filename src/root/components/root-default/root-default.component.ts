import {Component, OnInit, OnDestroy} from "@angular/core";
import {Subscription} from "rxjs";
import {RouterExtensions} from "nativescript-angular/router";
import {registerElement} from "nativescript-angular/element-registry";
import {DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition} from "nativescript-ui-sidedrawer";
import {NavigationProvider, UserInfoService, EcapLoginService, LocalPlatformDataService, AppSettingsProvider} from "@ecap3/tns-core";
import { IUserInfo } from "../../models/user.info";
import { navigations } from "~/root/navigation";
import {Page} from "tns-core-modules/ui/page";
import { CardView } from '@nstudio/nativescript-cardview';
import { Video } from 'nativescript-videoplayer';
import * as permissions from "nativescript-permissions";


registerElement('CardView', () => CardView);
registerElement("VideoPlayer", () => Video);


@Component({
    moduleId: module.id,
    selector: "ns-app",
    templateUrl: "root-default.component.html"
})
export class RootDefaultComponent implements OnInit, OnDestroy{

    userInfo: IUserInfo;
    navigations: any[];
    currentNavigations: any[];
    showNavigation = true;
    subscription: Subscription;
    private _sideDrawerTransition: DrawerTransitionBase;

    constructor(private routerExtensions: RouterExtensions,
        private navigationProvider: NavigationProvider,
        public ecapLoginService: EcapLoginService,
        private userInfoService: UserInfoService,
        private localPlatformDataService: LocalPlatformDataService,
        private appSettingProvider: AppSettingsProvider,
        private page: Page
    ) {
        console.log("!!! Fire RootDefaultComponent !!!");
        this.localPlatformDataService.initDatabase(this.appSettingProvider.getAppSettings().LocalDBName)
        this._sideDrawerTransition = new SlideInOnTopTransition();
        this.navigations = navigations;
    }

    get sideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    ngOnInit(): void {
        console.log("root default");
        // this.page.actionBarHidden = true;


        this.setEventListenerForUserInfo();

        this.navigationProvider.getCurrentNavigationObservable().subscribe(response => {
            console.log('navigation',this.currentNavigations);

            this.currentNavigations = response;
        });
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        //this.subscription.unsubscribe();
    }

    // Set Event Listener
    setEventListenerForUserInfo() {
        console.log("===== GetUpdatedUserData =============");
        //this.showNavigation = false;
        if (this.userInfoService.getUserInfo()) {
            //this.showNavigation = true;
            this.userInfoService.updateUserDetailsObserved();
        }

        // //console.log("Subscribe::UserDetailsObserved");
        // this.subscription = this.userInfoService.getUserDetailsObserved().subscribe(loginUserDetails => {
        //     //console.log("GetUserDetailsObserved::Subscription::");
        //     if (loginUserDetails && loginUserDetails.userDetails === {}) {
        //         this.showNavigation = false;
        //     } else if (loginUserDetails && loginUserDetails.userDetails) {
        //         this.userInfo.displayName = loginUserDetails.userDetails.DisplayName;
        //         this.userInfo.email = loginUserDetails.userDetails.Email;
        //     }
        // });

        //console.log("AddEventListener For GetUserInfoEvent In RootDefaultComponent");
        this.userInfoService.getUserInfoEvent().subscribe((userDetails: any) => {
            //console.log("============ RootDefaultComponent GetUserInfoEvent ===========");
            permissions
                .requestPermission([
                    android.Manifest.permission.CAMERA,
                    android.Manifest.permission.RECORD_AUDIO,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ]).then(output => {
                this.initiateRoute();
            });
        });
    }

    // Goto Initial route
    initiateRoute() {
        this.routerExtensions.navigate(["/dashboard"]);
    }
 }
