"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var nativescript_ui_sidedrawer_1 = require("nativescript-ui-sidedrawer");
var tns_core_1 = require("@ecap3/tns-core");
var navigation_1 = require("~/root/navigation");
var page_1 = require("tns-core-modules/ui/page");
var RootDefaultComponent = /** @class */ (function () {
    function RootDefaultComponent(routerExtensions, navigationProvider, ecapLoginService, userInfoService, page) {
        this.routerExtensions = routerExtensions;
        this.navigationProvider = navigationProvider;
        this.ecapLoginService = ecapLoginService;
        this.userInfoService = userInfoService;
        this.page = page;
        this.showNavigation = true;
        console.log("!!! Fire RootDefaultComponent !!!");
        this._sideDrawerTransition = new nativescript_ui_sidedrawer_1.SlideInOnTopTransition();
        this.navigations = navigation_1.navigations;
    }
    Object.defineProperty(RootDefaultComponent.prototype, "sideDrawerTransition", {
        get: function () {
            return this._sideDrawerTransition;
        },
        enumerable: true,
        configurable: true
    });
    RootDefaultComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log("root default");
        // this.page.actionBarHidden = true;
        this.setEventListenerForUserInfo();
        this.navigationProvider.getCurrentNavigationObserable().subscribe(function (response) {
            console.log('navigation', _this.currentNavigations);
            _this.currentNavigations = response;
        });
    };
    RootDefaultComponent.prototype.ngOnDestroy = function () {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    };
    // Set Event Listener 
    RootDefaultComponent.prototype.setEventListenerForUserInfo = function () {
        var _this = this;
        //console.log("===== GetUpdatedUserData =============");
        if (this.userInfoService.getUserInfo()) {
            this.userInfoService.updateUserDetailsObserved();
        }
        // //console.log("Subscribe::UserDetailsObserved");
        // this.subscription = this.userInfoService.getUserDetailsObserved().subscribe(loginUserDetails => {
        //     //console.log("GetUserDetailsObserved::Subscription::");
        //     if (loginUserDetails && loginUserDetails.userDetails === {}) {
        //         this.showNavigation = false;
        //     } else if (loginUserDetails && loginUserDetails.userDetails) {
        //         this.userInfo.displayName = loginUserDetails.userDetails.DisplayName;
        //         this.userInfo.email = loginUserDetails.userDetails.Email;
        //     }
        // });
        //console.log("AddEventListener For GetUserInfoEvent In RootDefaultComponent");
        this.userInfoService.getUserInfoEvent().subscribe(function (userDetails) {
            //console.log("============ RootDefaultComponent GetUserInfoEvent ===========");            
            _this.initiateRoute();
        });
    };
    // Goto Initial route
    RootDefaultComponent.prototype.initiateRoute = function () {
        this.routerExtensions.navigate(["/dashboard"]);
    };
    RootDefaultComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: "ns-app",
            templateUrl: "root-default.component.html"
        }),
        __metadata("design:paramtypes", [router_1.RouterExtensions,
            tns_core_1.NavigationProvider,
            tns_core_1.EcapLoginService,
            tns_core_1.UserInfoService,
            page_1.Page])
    ], RootDefaultComponent);
    return RootDefaultComponent;
}());
exports.RootDefaultComponent = RootDefaultComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm9vdC1kZWZhdWx0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInJvb3QtZGVmYXVsdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkQ7QUFFM0Qsc0RBQTZEO0FBQzdELHlFQUF1RztBQUN2Ryw0Q0FBc0Y7QUFFdEYsZ0RBQWdEO0FBQ2hELGlEQUE4QztBQVE5QztJQVNJLDhCQUFvQixnQkFBa0MsRUFDMUMsa0JBQXNDLEVBQ3ZDLGdCQUFrQyxFQUNqQyxlQUFnQyxFQUNoQyxJQUFVO1FBSkYscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUMxQyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3ZDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDakMsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQ2hDLFNBQUksR0FBSixJQUFJLENBQU07UUFSdEIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFVbEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLG1EQUFzQixFQUFFLENBQUM7UUFDMUQsSUFBSSxDQUFDLFdBQVcsR0FBRyx3QkFBVyxDQUFDO0lBQ25DLENBQUM7SUFFRCxzQkFBSSxzREFBb0I7YUFBeEI7WUFDSSxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztRQUN0QyxDQUFDOzs7T0FBQTtJQUVELHVDQUFRLEdBQVI7UUFBQSxpQkFXQztRQVZHLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDNUIsb0NBQW9DO1FBRXBDLElBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFDO1FBRW5DLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyw2QkFBNkIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDdEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFFbEQsS0FBSSxDQUFDLGtCQUFrQixHQUFHLFFBQVEsQ0FBQztRQUN2QyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwwQ0FBVyxHQUFYO1FBQ0ksd0NBQXdDO1FBQ3hDLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDcEMsQ0FBQztJQUVELHNCQUFzQjtJQUN0QiwwREFBMkIsR0FBM0I7UUFBQSxpQkFzQkM7UUFyQkcsd0RBQXdEO1FBQ3hELElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxXQUFXLEVBQUUsRUFBRTtZQUNwQyxJQUFJLENBQUMsZUFBZSxDQUFDLHlCQUF5QixFQUFFLENBQUM7U0FDcEQ7UUFFRCxtREFBbUQ7UUFDbkQsb0dBQW9HO1FBQ3BHLCtEQUErRDtRQUMvRCxxRUFBcUU7UUFDckUsdUNBQXVDO1FBQ3ZDLHFFQUFxRTtRQUNyRSxnRkFBZ0Y7UUFDaEYsb0VBQW9FO1FBQ3BFLFFBQVE7UUFDUixNQUFNO1FBRU4sK0VBQStFO1FBQy9FLElBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxTQUFTLENBQUMsVUFBQyxXQUFnQjtZQUMvRCw0RkFBNEY7WUFDNUYsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHFCQUFxQjtJQUNyQiw0Q0FBYSxHQUFiO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQXRFUSxvQkFBb0I7UUFMaEMsZ0JBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtZQUNuQixRQUFRLEVBQUUsUUFBUTtZQUNsQixXQUFXLEVBQUUsNkJBQTZCO1NBQzdDLENBQUM7eUNBVXdDLHlCQUFnQjtZQUN0Qiw2QkFBa0I7WUFDckIsMkJBQWdCO1lBQ2hCLDBCQUFlO1lBQzFCLFdBQUk7T0FiYixvQkFBb0IsQ0F1RS9CO0lBQUQsMkJBQUM7Q0FBQSxBQXZFRixJQXVFRTtBQXZFVyxvREFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0NvbXBvbmVudCwgT25Jbml0LCBPbkRlc3Ryb3l9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7U3Vic2NyaXB0aW9ufSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQge1JvdXRlckV4dGVuc2lvbnN9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHtEcmF3ZXJUcmFuc2l0aW9uQmFzZSwgUmFkU2lkZURyYXdlciwgU2xpZGVJbk9uVG9wVHJhbnNpdGlvbn0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1zaWRlZHJhd2VyXCI7XHJcbmltcG9ydCB7TmF2aWdhdGlvblByb3ZpZGVyLCBVc2VySW5mb1NlcnZpY2UsIEVjYXBMb2dpblNlcnZpY2V9IGZyb20gXCJAZWNhcDMvdG5zLWNvcmVcIjtcclxuaW1wb3J0IHsgSVVzZXJJbmZvIH0gZnJvbSBcIi4uLy4uL21vZGVscy91c2VyLmluZm9cIjtcclxuaW1wb3J0IHsgbmF2aWdhdGlvbnMgfSBmcm9tIFwifi9yb290L25hdmlnYXRpb25cIjtcclxuaW1wb3J0IHtQYWdlfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9wYWdlXCI7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxyXG4gICAgc2VsZWN0b3I6IFwibnMtYXBwXCIsXHJcbiAgICB0ZW1wbGF0ZVVybDogXCJyb290LWRlZmF1bHQuY29tcG9uZW50Lmh0bWxcIlxyXG59KVxyXG5leHBvcnQgY2xhc3MgUm9vdERlZmF1bHRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveXtcclxuXHJcbiAgICB1c2VySW5mbzogSVVzZXJJbmZvO1xyXG4gICAgbmF2aWdhdGlvbnM6IGFueVtdO1xyXG4gICAgY3VycmVudE5hdmlnYXRpb25zOiBhbnlbXTtcclxuICAgIHNob3dOYXZpZ2F0aW9uID0gdHJ1ZTtcclxuICAgIHN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG4gICAgcHJpdmF0ZSBfc2lkZURyYXdlclRyYW5zaXRpb246IERyYXdlclRyYW5zaXRpb25CYXNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyRXh0ZW5zaW9uczogUm91dGVyRXh0ZW5zaW9ucyxcclxuICAgICAgICBwcml2YXRlIG5hdmlnYXRpb25Qcm92aWRlcjogTmF2aWdhdGlvblByb3ZpZGVyLCBcclxuICAgICAgICBwdWJsaWMgZWNhcExvZ2luU2VydmljZTogRWNhcExvZ2luU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIHVzZXJJbmZvU2VydmljZTogVXNlckluZm9TZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgcGFnZTogUGFnZVxyXG4gICAgKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCIhISEgRmlyZSBSb290RGVmYXVsdENvbXBvbmVudCAhISFcIik7XHJcbiAgICAgICAgdGhpcy5fc2lkZURyYXdlclRyYW5zaXRpb24gPSBuZXcgU2xpZGVJbk9uVG9wVHJhbnNpdGlvbigpO1xyXG4gICAgICAgIHRoaXMubmF2aWdhdGlvbnMgPSBuYXZpZ2F0aW9ucztcclxuICAgIH1cclxuXHJcbiAgICBnZXQgc2lkZURyYXdlclRyYW5zaXRpb24oKTogRHJhd2VyVHJhbnNpdGlvbkJhc2Uge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zaWRlRHJhd2VyVHJhbnNpdGlvbjtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcInJvb3QgZGVmYXVsdFwiKTtcclxuICAgICAgICAvLyB0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLnNldEV2ZW50TGlzdGVuZXJGb3JVc2VySW5mbygpO1xyXG5cclxuICAgICAgICB0aGlzLm5hdmlnYXRpb25Qcm92aWRlci5nZXRDdXJyZW50TmF2aWdhdGlvbk9ic2VyYWJsZSgpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCduYXZpZ2F0aW9uJyx0aGlzLmN1cnJlbnROYXZpZ2F0aW9ucyk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnROYXZpZ2F0aW9ucyA9IHJlc3BvbnNlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIC8vIHVuc3Vic2NyaWJlIHRvIGVuc3VyZSBubyBtZW1vcnkgbGVha3NcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFNldCBFdmVudCBMaXN0ZW5lciBcclxuICAgIHNldEV2ZW50TGlzdGVuZXJGb3JVc2VySW5mbygpIHtcclxuICAgICAgICAvL2NvbnNvbGUubG9nKFwiPT09PT0gR2V0VXBkYXRlZFVzZXJEYXRhID09PT09PT09PT09PT1cIik7XHJcbiAgICAgICAgaWYgKHRoaXMudXNlckluZm9TZXJ2aWNlLmdldFVzZXJJbmZvKCkpIHtcclxuICAgICAgICAgICAgdGhpcy51c2VySW5mb1NlcnZpY2UudXBkYXRlVXNlckRldGFpbHNPYnNlcnZlZCgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gLy9jb25zb2xlLmxvZyhcIlN1YnNjcmliZTo6VXNlckRldGFpbHNPYnNlcnZlZFwiKTtcclxuICAgICAgICAvLyB0aGlzLnN1YnNjcmlwdGlvbiA9IHRoaXMudXNlckluZm9TZXJ2aWNlLmdldFVzZXJEZXRhaWxzT2JzZXJ2ZWQoKS5zdWJzY3JpYmUobG9naW5Vc2VyRGV0YWlscyA9PiB7XHJcbiAgICAgICAgLy8gICAgIC8vY29uc29sZS5sb2coXCJHZXRVc2VyRGV0YWlsc09ic2VydmVkOjpTdWJzY3JpcHRpb246OlwiKTtcclxuICAgICAgICAvLyAgICAgaWYgKGxvZ2luVXNlckRldGFpbHMgJiYgbG9naW5Vc2VyRGV0YWlscy51c2VyRGV0YWlscyA9PT0ge30pIHtcclxuICAgICAgICAvLyAgICAgICAgIHRoaXMuc2hvd05hdmlnYXRpb24gPSBmYWxzZTtcclxuICAgICAgICAvLyAgICAgfSBlbHNlIGlmIChsb2dpblVzZXJEZXRhaWxzICYmIGxvZ2luVXNlckRldGFpbHMudXNlckRldGFpbHMpIHtcclxuICAgICAgICAvLyAgICAgICAgIHRoaXMudXNlckluZm8uZGlzcGxheU5hbWUgPSBsb2dpblVzZXJEZXRhaWxzLnVzZXJEZXRhaWxzLkRpc3BsYXlOYW1lO1xyXG4gICAgICAgIC8vICAgICAgICAgdGhpcy51c2VySW5mby5lbWFpbCA9IGxvZ2luVXNlckRldGFpbHMudXNlckRldGFpbHMuRW1haWw7XHJcbiAgICAgICAgLy8gICAgIH1cclxuICAgICAgICAvLyB9KTtcclxuXHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyhcIkFkZEV2ZW50TGlzdGVuZXIgRm9yIEdldFVzZXJJbmZvRXZlbnQgSW4gUm9vdERlZmF1bHRDb21wb25lbnRcIik7XHJcbiAgICAgICAgdGhpcy51c2VySW5mb1NlcnZpY2UuZ2V0VXNlckluZm9FdmVudCgpLnN1YnNjcmliZSgodXNlckRldGFpbHM6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiPT09PT09PT09PT09IFJvb3REZWZhdWx0Q29tcG9uZW50IEdldFVzZXJJbmZvRXZlbnQgPT09PT09PT09PT1cIik7ICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHRoaXMuaW5pdGlhdGVSb3V0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEdvdG8gSW5pdGlhbCByb3V0ZVxyXG4gICAgaW5pdGlhdGVSb3V0ZSgpIHtcclxuICAgICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2Rhc2hib2FyZFwiXSk7XHJcbiAgICB9XHJcbiB9XHJcbiJdfQ==