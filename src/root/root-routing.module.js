"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var tns_core_1 = require("@ecap3/tns-core");
// const routes: Routes = [
//     { path: "", redirectTo: "/dashboard", pathMatch: "full" },
//     { path: "dashboard", loadChildren: "~/app/dashboard/dashboard.module#DashboardModule" }
// ];
var routes = [
    { path: "", redirectTo: "/dashboard", pathMatch: "full" },
    {
        path: "login",
        loadChildren: "~/app/login/login.module#LoginModule",
        data: {
            requiredFeature: 'login',
            authFailRedirection: '/login'
        }
    },
    {
        path: "dashboard",
        loadChildren: "~/app/dashboard/dashboard.module#DashboardModule",
        canActivate: [tns_core_1.AuthGuard],
        data: {
            requiredFeature: 'measure',
            authFailRedirection: '/login'
        }
    },
    {
        path: "project",
        loadChildren: "~/app/project/project.module#ProjectModule",
        canActivate: [tns_core_1.AuthGuard],
        data: {
            requiredFeature: 'measure',
            authFailRedirection: '/login'
        }
    }
];
var RootRoutingModule = /** @class */ (function () {
    function RootRoutingModule() {
    }
    RootRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.NativeScriptRouterModule.forRoot(routes)],
            exports: [router_1.NativeScriptRouterModule]
        })
    ], RootRoutingModule);
    return RootRoutingModule;
}());
exports.RootRoutingModule = RootRoutingModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm9vdC1yb3V0aW5nLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInJvb3Qtcm91dGluZy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBeUM7QUFFekMsc0RBQXVFO0FBQ3ZFLDRDQUE0QztBQUU1QywyQkFBMkI7QUFDM0IsaUVBQWlFO0FBQ2pFLDhGQUE4RjtBQUM5RixLQUFLO0FBRUwsSUFBTSxNQUFNLEdBQVc7SUFDbkIsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRTtJQUN6RDtRQUNJLElBQUksRUFBRSxPQUFPO1FBQ2IsWUFBWSxFQUFFLHNDQUFzQztRQUNwRCxJQUFJLEVBQUU7WUFDRixlQUFlLEVBQUUsT0FBTztZQUN4QixtQkFBbUIsRUFBRSxRQUFRO1NBQ2hDO0tBQ0o7SUFDRDtRQUNJLElBQUksRUFBRSxXQUFXO1FBQ2pCLFlBQVksRUFBRSxrREFBa0Q7UUFDaEUsV0FBVyxFQUFFLENBQUMsb0JBQVMsQ0FBQztRQUN4QixJQUFJLEVBQUU7WUFDRixlQUFlLEVBQUUsU0FBUztZQUMxQixtQkFBbUIsRUFBRSxRQUFRO1NBQ2hDO0tBQ0o7SUFDRDtRQUNJLElBQUksRUFBRSxTQUFTO1FBQ2YsWUFBWSxFQUFFLDRDQUE0QztRQUMxRCxXQUFXLEVBQUUsQ0FBQyxvQkFBUyxDQUFDO1FBQ3hCLElBQUksRUFBRTtZQUNGLGVBQWUsRUFBRSxTQUFTO1lBQzFCLG1CQUFtQixFQUFFLFFBQVE7U0FDaEM7S0FDSjtDQUNKLENBQUM7QUFNRjtJQUFBO0lBRUEsQ0FBQztJQUZZLGlCQUFpQjtRQUo3QixlQUFRLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQyxpQ0FBd0IsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbkQsT0FBTyxFQUFFLENBQUMsaUNBQXdCLENBQUM7U0FDdEMsQ0FBQztPQUNXLGlCQUFpQixDQUU3QjtJQUFELHdCQUFDO0NBQUEsQUFGRCxJQUVDO0FBRlksOENBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXMgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFJvdXRlck1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgQXV0aEd1YXJkIH0gZnJvbSBcIkBlY2FwMy90bnMtY29yZVwiO1xyXG5cclxuLy8gY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXHJcbi8vICAgICB7IHBhdGg6IFwiXCIsIHJlZGlyZWN0VG86IFwiL2Rhc2hib2FyZFwiLCBwYXRoTWF0Y2g6IFwiZnVsbFwiIH0sXHJcbi8vICAgICB7IHBhdGg6IFwiZGFzaGJvYXJkXCIsIGxvYWRDaGlsZHJlbjogXCJ+L2FwcC9kYXNoYm9hcmQvZGFzaGJvYXJkLm1vZHVsZSNEYXNoYm9hcmRNb2R1bGVcIiB9XHJcbi8vIF07XHJcblxyXG5jb25zdCByb3V0ZXM6IFJvdXRlcyA9IFtcclxuICAgIHsgcGF0aDogXCJcIiwgcmVkaXJlY3RUbzogXCIvZGFzaGJvYXJkXCIsIHBhdGhNYXRjaDogXCJmdWxsXCIgfSxcclxuICAgIHtcclxuICAgICAgICBwYXRoOiBcImxvZ2luXCIsXHJcbiAgICAgICAgbG9hZENoaWxkcmVuOiBcIn4vYXBwL2xvZ2luL2xvZ2luLm1vZHVsZSNMb2dpbk1vZHVsZVwiLFxyXG4gICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgcmVxdWlyZWRGZWF0dXJlOiAnbG9naW4nLFxyXG4gICAgICAgICAgICBhdXRoRmFpbFJlZGlyZWN0aW9uOiAnL2xvZ2luJ1xyXG4gICAgICAgIH1cclxuICAgIH0sXHJcbiAgICB7IFxyXG4gICAgICAgIHBhdGg6IFwiZGFzaGJvYXJkXCIsIFxyXG4gICAgICAgIGxvYWRDaGlsZHJlbjogXCJ+L2FwcC9kYXNoYm9hcmQvZGFzaGJvYXJkLm1vZHVsZSNEYXNoYm9hcmRNb2R1bGVcIixcclxuICAgICAgICBjYW5BY3RpdmF0ZTogW0F1dGhHdWFyZF0sXHJcbiAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICByZXF1aXJlZEZlYXR1cmU6ICdtZWFzdXJlJyxcclxuICAgICAgICAgICAgYXV0aEZhaWxSZWRpcmVjdGlvbjogJy9sb2dpbidcclxuICAgICAgICB9XHJcbiAgICB9LFxyXG4gICAgeyBcclxuICAgICAgICBwYXRoOiBcInByb2plY3RcIiwgXHJcbiAgICAgICAgbG9hZENoaWxkcmVuOiBcIn4vYXBwL3Byb2plY3QvcHJvamVjdC5tb2R1bGUjUHJvamVjdE1vZHVsZVwiLFxyXG4gICAgICAgIGNhbkFjdGl2YXRlOiBbQXV0aEd1YXJkXSxcclxuICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgIHJlcXVpcmVkRmVhdHVyZTogJ21lYXN1cmUnLFxyXG4gICAgICAgICAgICBhdXRoRmFpbFJlZGlyZWN0aW9uOiAnL2xvZ2luJ1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXTtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlLmZvclJvb3Qocm91dGVzKV0sXHJcbiAgICBleHBvcnRzOiBbTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUm9vdFJvdXRpbmdNb2R1bGUgeyBcclxuICAgIFxyXG59XHJcbiJdfQ==