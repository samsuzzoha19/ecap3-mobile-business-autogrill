"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("nativescript-angular/common");
var http_client_1 = require("nativescript-angular/http-client");
var nativescript_module_1 = require("nativescript-angular/nativescript.module");
var angular_1 = require("nativescript-localize/angular");
var angular_2 = require("nativescript-ui-sidedrawer/angular");
var tns_core_1 = require("@ecap3/tns-core");
var tns_core_template_1 = require("@ecap3/tns-core-template");
var root_routing_module_1 = require("./root-routing.module");
var root_default_component_1 = require("./components/root-default/root-default.component");
var navigation_1 = require("./navigation");
var app_setting_service_1 = require("./services/configs/app-setting.service");
var nativescript_ngx_fonticon_1 = require("nativescript-ngx-fonticon");
var RootModule = /** @class */ (function () {
    function RootModule(navigationProvider, appSettingsProvider, appSettingService) {
        this.navigationProvider = navigationProvider;
        this.appSettingsProvider = appSettingsProvider;
        this.appSettingService = appSettingService;
        console.log("============ RootModule SET Navigations ===========");
        this.navigationProvider.setCurrentNavigations(navigation_1.navigations);
        console.log("============ RootModule SET AppSettings ===========");
        this.appSettingsProvider.setAppSettings(this.appSettingService.getAppSettings());
    }
    RootModule = __decorate([
        core_1.NgModule({
            bootstrap: [
                root_default_component_1.RootDefaultComponent
            ],
            imports: [
                nativescript_module_1.NativeScriptModule,
                root_routing_module_1.RootRoutingModule,
                common_1.NativeScriptCommonModule,
                http_client_1.NativeScriptHttpClientModule,
                angular_1.NativeScriptLocalizeModule,
                angular_2.NativeScriptUISideDrawerModule,
                tns_core_1.EcapCoreModule.forRoot(),
                tns_core_template_1.CoreTemplateModule,
                nativescript_ngx_fonticon_1.TNSFontIconModule.forRoot({
                    'fa': './../fonts/font-awesome.css'
                })
            ],
            providers: [
                app_setting_service_1.AppSettingService
            ],
            declarations: [
                root_default_component_1.RootDefaultComponent
            ],
            schemas: [
                core_1.NO_ERRORS_SCHEMA
            ]
        }),
        __metadata("design:paramtypes", [tns_core_1.NavigationProvider,
            tns_core_1.AppSettingsProvider,
            app_setting_service_1.AppSettingService])
    ], RootModule);
    return RootModule;
}());
exports.RootModule = RootModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm9vdC5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJyb290Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUEyRDtBQUMzRCxzREFBdUU7QUFDdkUsZ0VBQWdGO0FBQ2hGLGdGQUE4RTtBQUM5RSx5REFBMkU7QUFDM0UsOERBQW9GO0FBRXBGLDRDQUEwRjtBQUMxRiw4REFBOEQ7QUFFOUQsNkRBQTBEO0FBQzFELDJGQUF3RjtBQUV4RiwyQ0FBMkM7QUFDM0MsOEVBQTJFO0FBQzNFLHVFQUE4RDtBQThCOUQ7SUFFSSxvQkFDWSxrQkFBc0MsRUFDdEMsbUJBQXdDLEVBQ3hDLGlCQUFvQztRQUZwQyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUc1QyxPQUFPLENBQUMsR0FBRyxDQUFDLHFEQUFxRCxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHFCQUFxQixDQUFDLHdCQUFXLENBQUMsQ0FBQztRQUUzRCxPQUFPLENBQUMsR0FBRyxDQUFDLHFEQUFxRCxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQztJQUNyRixDQUFDO0lBYlEsVUFBVTtRQTNCdEIsZUFBUSxDQUFDO1lBQ04sU0FBUyxFQUFFO2dCQUNQLDZDQUFvQjthQUN2QjtZQUNELE9BQU8sRUFBRTtnQkFDTCx3Q0FBa0I7Z0JBQ2xCLHVDQUFpQjtnQkFDakIsaUNBQXdCO2dCQUN4QiwwQ0FBNEI7Z0JBQzVCLG9DQUEwQjtnQkFDMUIsd0NBQThCO2dCQUM5Qix5QkFBYyxDQUFDLE9BQU8sRUFBRTtnQkFDeEIsc0NBQWtCO2dCQUNsQiw2Q0FBaUIsQ0FBQyxPQUFPLENBQUM7b0JBQy9CLElBQUksRUFBRSw2QkFBNkI7aUJBQ25DLENBQUM7YUFDQztZQUNELFNBQVMsRUFBRTtnQkFDUCx1Q0FBaUI7YUFDcEI7WUFDRCxZQUFZLEVBQUU7Z0JBQ1YsNkNBQW9CO2FBQ3ZCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLHVCQUFnQjthQUNuQjtTQUNKLENBQUM7eUNBSWtDLDZCQUFrQjtZQUNqQiw4QkFBbUI7WUFDckIsdUNBQWlCO09BTHZDLFVBQVUsQ0FjdEI7SUFBRCxpQkFBQztDQUFBLEFBZEQsSUFjQztBQWRZLGdDQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE5PX0VSUk9SU19TQ0hFTUEgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvY29tbW9uXCI7XHJcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvaHR0cC1jbGllbnRcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL25hdGl2ZXNjcmlwdC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0TG9jYWxpemVNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWxvY2FsaXplL2FuZ3VsYXJcIjtcclxuaW1wb3J0IHsgTmF0aXZlU2NyaXB0VUlTaWRlRHJhd2VyTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1zaWRlZHJhd2VyL2FuZ3VsYXJcIjtcclxuXHJcbmltcG9ydCB7IEVjYXBDb3JlTW9kdWxlLCBOYXZpZ2F0aW9uUHJvdmlkZXIsIEFwcFNldHRpbmdzUHJvdmlkZXIgfSBmcm9tIFwiQGVjYXAzL3Rucy1jb3JlXCI7XHJcbmltcG9ydCB7IENvcmVUZW1wbGF0ZU1vZHVsZSB9IGZyb20gXCJAZWNhcDMvdG5zLWNvcmUtdGVtcGxhdGVcIjtcclxuXHJcbmltcG9ydCB7IFJvb3RSb3V0aW5nTW9kdWxlIH0gZnJvbSBcIi4vcm9vdC1yb3V0aW5nLm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBSb290RGVmYXVsdENvbXBvbmVudCB9IGZyb20gXCIuL2NvbXBvbmVudHMvcm9vdC1kZWZhdWx0L3Jvb3QtZGVmYXVsdC5jb21wb25lbnRcIjtcclxuXHJcbmltcG9ydCB7IG5hdmlnYXRpb25zIH0gZnJvbSBcIi4vbmF2aWdhdGlvblwiO1xyXG5pbXBvcnQgeyBBcHBTZXR0aW5nU2VydmljZSB9IGZyb20gXCIuL3NlcnZpY2VzL2NvbmZpZ3MvYXBwLXNldHRpbmcuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBUTlNGb250SWNvbk1vZHVsZSB9IGZyb20gJ25hdGl2ZXNjcmlwdC1uZ3gtZm9udGljb24nO1xyXG5cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBib290c3RyYXA6IFtcclxuICAgICAgICBSb290RGVmYXVsdENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBOYXRpdmVTY3JpcHRNb2R1bGUsXHJcbiAgICAgICAgUm9vdFJvdXRpbmdNb2R1bGUsXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0Q29tbW9uTW9kdWxlLFxyXG4gICAgICAgIE5hdGl2ZVNjcmlwdEh0dHBDbGllbnRNb2R1bGUsXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0TG9jYWxpemVNb2R1bGUsXHJcbiAgICAgICAgTmF0aXZlU2NyaXB0VUlTaWRlRHJhd2VyTW9kdWxlLFxyXG4gICAgICAgIEVjYXBDb3JlTW9kdWxlLmZvclJvb3QoKSxcclxuICAgICAgICBDb3JlVGVtcGxhdGVNb2R1bGUsXHJcbiAgICAgICAgVE5TRm9udEljb25Nb2R1bGUuZm9yUm9vdCh7XHJcblx0XHRcdCdmYSc6ICcuLy4uL2ZvbnRzL2ZvbnQtYXdlc29tZS5jc3MnXHJcblx0XHR9KVxyXG4gICAgXSxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIEFwcFNldHRpbmdTZXJ2aWNlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgUm9vdERlZmF1bHRDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBzY2hlbWFzOiBbXHJcbiAgICAgICAgTk9fRVJST1JTX1NDSEVNQVxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUm9vdE1vZHVsZSB7XHJcbiAgICBcclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgbmF2aWdhdGlvblByb3ZpZGVyOiBOYXZpZ2F0aW9uUHJvdmlkZXIsIFxyXG4gICAgICAgIHByaXZhdGUgYXBwU2V0dGluZ3NQcm92aWRlcjogQXBwU2V0dGluZ3NQcm92aWRlcixcclxuICAgICAgICBwcml2YXRlIGFwcFNldHRpbmdTZXJ2aWNlOiBBcHBTZXR0aW5nU2VydmljZVxyXG4gICAgKSBcclxuICAgIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIj09PT09PT09PT09PSBSb290TW9kdWxlIFNFVCBOYXZpZ2F0aW9ucyA9PT09PT09PT09PVwiKTtcclxuICAgICAgICB0aGlzLm5hdmlnYXRpb25Qcm92aWRlci5zZXRDdXJyZW50TmF2aWdhdGlvbnMobmF2aWdhdGlvbnMpO1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhcIj09PT09PT09PT09PSBSb290TW9kdWxlIFNFVCBBcHBTZXR0aW5ncyA9PT09PT09PT09PVwiKTtcclxuICAgICAgICB0aGlzLmFwcFNldHRpbmdzUHJvdmlkZXIuc2V0QXBwU2V0dGluZ3ModGhpcy5hcHBTZXR0aW5nU2VydmljZS5nZXRBcHBTZXR0aW5ncygpKTtcclxuICAgIH1cclxufVxyXG4iXX0=