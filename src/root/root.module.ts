import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptLocalizeModule } from "nativescript-localize/angular";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

import { EcapCoreModule, NavigationProvider, AppSettingsProvider } from "@ecap3/tns-core";
import { CoreTemplateModule } from "@ecap3/tns-core-template";

import { RootRoutingModule } from "./root-routing.module";
import { RootDefaultComponent } from "./components/root-default/root-default.component";

import { navigations } from "./navigation";
import { AppSettingService } from "./services/configs/app-setting.service";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';


@NgModule({
    bootstrap: [
        RootDefaultComponent
    ],
    imports: [
        NativeScriptModule,
        RootRoutingModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NativeScriptLocalizeModule,
        NativeScriptUISideDrawerModule,
        NativeScriptUIListViewModule,
        EcapCoreModule.forRoot(),
        CoreTemplateModule,
        TNSFontIconModule.forRoot({
            'fa': './../fonts/font-awesome.css',
            'md-icon': './../fonts/material-icons.css'
		})
    ],
    providers: [
        AppSettingService
    ],
    declarations: [
        RootDefaultComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class RootModule {
    
    constructor(
        private navigationProvider: NavigationProvider, 
        private appSettingsProvider: AppSettingsProvider,
        private appSettingService: AppSettingService
    ) 
    {
        console.log("============ RootModule SET Navigations ===========");
        this.navigationProvider.setCurrentNavigations(navigations);

        console.log("============ RootModule SET AppSettings ===========");
        this.appSettingsProvider.setAppSettings(this.appSettingService.getAppSettings());
    }
}
