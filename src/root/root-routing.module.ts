import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AuthGuard } from "@ecap3/tns-core";

// const routes: Routes = [
//     { path: "", redirectTo: "/dashboard", pathMatch: "full" },
//     { path: "dashboard", loadChildren: "~/app/dashboard/dashboard.module#DashboardModule" }
// ];

const routes: Routes = [
    { path: "", redirectTo: "/dashboard", pathMatch: "full" },
    {
        path: "login",
        loadChildren: "~/app/login/login.module#LoginModule",
        data: {
            requiredFeature: 'login',
            authFailRedirection: '/login'
        }
    },
    {
        path: "dashboard",
        loadChildren: "~/app/dashboard/dashboard.module#DashboardModule",
        canActivate: [AuthGuard],
        data: {
            requiredFeature: 'profile',
            authFailRedirection: '/login'
        }
    },
    {
        path: "project",
        loadChildren: "~/app/project/project.module#ProjectModule",
        canActivate: [AuthGuard],
        data: {
            requiredFeature: 'profile',
            authFailRedirection: '/login'
        }
    },
    {
        path: "test",
        loadChildren: "~/app/test/test.module#TestModule",
        canActivate: [AuthGuard],
        data: {
            requiredFeature: 'profile',
            authFailRedirection: '/login'
        }
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class RootRoutingModule {

}
