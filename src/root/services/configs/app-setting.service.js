"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var environment_1 = require("~/environments/environment");
var AppSettingService = /** @class */ (function () {
    function AppSettingService() {
    }
    AppSettingService.prototype.getAppSettings = function () {
        var appSettings = {
            Apps: environment_1.environment.Apps,
            Token: environment_1.environment.Token,
            Security: environment_1.environment.Security,
            Identity: environment_1.environment.Identity,
            Navigation: environment_1.environment.Navigations,
            TenantId: environment_1.environment.TenantId,
            Origin: environment_1.environment.Origin,
            DomainName: environment_1.environment.DomainName,
            SubDomainName: environment_1.environment.SubDomainName,
            Authentication: environment_1.environment.Authentication,
            DataCore: environment_1.environment.DataCore,
            IsMock: false
        };
        return appSettings;
    };
    AppSettingService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], AppSettingService);
    return AppSettingService;
}());
exports.AppSettingService = AppSettingService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLXNldHRpbmcuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC1zZXR0aW5nLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBMkM7QUFFM0MsMERBQXlEO0FBR3pEO0lBRUk7SUFDQSxDQUFDO0lBRUQsMENBQWMsR0FBZDtRQUVJLElBQUksV0FBVyxHQUFpQjtZQUM1QixJQUFJLEVBQUUseUJBQVcsQ0FBQyxJQUFJO1lBQ3RCLEtBQUssRUFBRSx5QkFBVyxDQUFDLEtBQUs7WUFDeEIsUUFBUSxFQUFFLHlCQUFXLENBQUMsUUFBUTtZQUM5QixRQUFRLEVBQUUseUJBQVcsQ0FBQyxRQUFRO1lBQzlCLFVBQVUsRUFBRSx5QkFBVyxDQUFDLFdBQVc7WUFDbkMsUUFBUSxFQUFFLHlCQUFXLENBQUMsUUFBUTtZQUM5QixNQUFNLEVBQUUseUJBQVcsQ0FBQyxNQUFNO1lBQzFCLFVBQVUsRUFBRSx5QkFBVyxDQUFDLFVBQVU7WUFDbEMsYUFBYSxFQUFFLHlCQUFXLENBQUMsYUFBYTtZQUN4QyxjQUFjLEVBQUUseUJBQVcsQ0FBQyxjQUFjO1lBQzFDLFFBQVEsRUFBRSx5QkFBVyxDQUFDLFFBQVE7WUFDOUIsTUFBTSxFQUFFLEtBQUs7U0FDaEIsQ0FBQTtRQUVELE9BQU8sV0FBVyxDQUFDO0lBQ3ZCLENBQUM7SUF2QlEsaUJBQWlCO1FBRDdCLGlCQUFVLEVBQUU7O09BQ0EsaUJBQWlCLENBd0I3QjtJQUFELHdCQUFDO0NBQUEsQUF4QkQsSUF3QkM7QUF4QlksOENBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBJQXBwU2V0dGluZ3MgfSBmcm9tIFwiQGVjYXAzL3Rucy1jb3JlXCI7XHJcbmltcG9ydCB7IGVudmlyb25tZW50IH0gZnJvbSBcIn4vZW52aXJvbm1lbnRzL2Vudmlyb25tZW50XCI7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBcHBTZXR0aW5nU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QXBwU2V0dGluZ3MoKTogSUFwcFNldHRpbmdzIHtcclxuXHJcbiAgICAgICAgdmFyIGFwcFNldHRpbmdzOiBJQXBwU2V0dGluZ3MgPSB7XHJcbiAgICAgICAgICAgIEFwcHM6IGVudmlyb25tZW50LkFwcHMsXHJcbiAgICAgICAgICAgIFRva2VuOiBlbnZpcm9ubWVudC5Ub2tlbixcclxuICAgICAgICAgICAgU2VjdXJpdHk6IGVudmlyb25tZW50LlNlY3VyaXR5LFxyXG4gICAgICAgICAgICBJZGVudGl0eTogZW52aXJvbm1lbnQuSWRlbnRpdHksXHJcbiAgICAgICAgICAgIE5hdmlnYXRpb246IGVudmlyb25tZW50Lk5hdmlnYXRpb25zLFxyXG4gICAgICAgICAgICBUZW5hbnRJZDogZW52aXJvbm1lbnQuVGVuYW50SWQsXHJcbiAgICAgICAgICAgIE9yaWdpbjogZW52aXJvbm1lbnQuT3JpZ2luLFxyXG4gICAgICAgICAgICBEb21haW5OYW1lOiBlbnZpcm9ubWVudC5Eb21haW5OYW1lLFxyXG4gICAgICAgICAgICBTdWJEb21haW5OYW1lOiBlbnZpcm9ubWVudC5TdWJEb21haW5OYW1lLFxyXG4gICAgICAgICAgICBBdXRoZW50aWNhdGlvbjogZW52aXJvbm1lbnQuQXV0aGVudGljYXRpb24sXHJcbiAgICAgICAgICAgIERhdGFDb3JlOiBlbnZpcm9ubWVudC5EYXRhQ29yZSxcclxuICAgICAgICAgICAgSXNNb2NrOiBmYWxzZVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGFwcFNldHRpbmdzO1xyXG4gICAgfVxyXG59Il19